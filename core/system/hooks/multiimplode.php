<?php
function multiimplode($delimiters, $string) {
   if (empty($string))
   return $string;

   if (is_array($delimiters)) {
      foreach($delimiters as $delim) {
         if (is_array($string)) {
            foreach ($string as $sub) {
               $aux[] = multiimplode($delim, $sub);
            }

            $string = $aux;
         }
         else $string = implode($delim, $string);
      }
   }
   else {
      echo $delimiters;
      if (is_array($string)) {
         foreach ($string as $sub) {
            $aux[] = multiimplode($delimiters, $sub);
         }

         $string = $aux;
      }
      else $string = implode($delimiters, $string);
   }
}

