<?php
function ellipsize($phrase, $sizeTo=false) {
   if (!$sizeTo)
   return $phrase;

   $words = explode(' ', $phrase);
   $reduce= false;

   while(strlen($phrase) > $sizeTo) {
      $reduce = true;
      array_pop($words);
      $phrase = implode(' ', $words) . '...';
   }

   return $phrase;
}
