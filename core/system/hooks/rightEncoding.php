<?php
function rightEncoding($text, $encType='UTF-8') { 
   $current_encoding = mb_detect_encoding($text, 'auto');

   if ($current_encoding != $encType)
   $text = iconv($current_encoding, $encType, $text);

   return $text;
}
