<?php
function logEvent($title=false, $data, $filename=false, $toConsole=false, $newLine=true, $append=true) {
   if (is_array($data))
   $data = json_encode($data, JSON_UNESCAPED_UNICODE);

   if (strlen($title))
   $title = "( $title ) ";

   $file = (!empty($filename)) ? $filename : RF_LOGFILE;
   $data = ($newLine) ? "\r\n[".Date('Y/m/d H:i:s')."] ".$title.$data : $title.$data;

   $append = ($append) ? FILE_APPEND : false;

   file_put_contents(RF_LOGPATH.'/'.$file, $data, $append);

   if ($toConsole) {
      if (SystemHandler::getInstance()->isCliRequest())
           echo $data;
      else echo "<BR>$data";
   }
}
