<?php
function getFilesFromDir($dir, $needed=0) {
   $file = false;

   if (is_dir($dir)) {
      if ($h = opendir($dir)) {
         $pos = 0;

         while (($file = readdir($h)) !== false) {
            if ($file != '.' && $file != '..') {
               $pos++;
  
               if ($pos == $needed) {
                  break;
               }
            }
         }
         closedir($h);
      }
   }

   return $file;
}
