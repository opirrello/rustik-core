<?php
function dump($var, $return=false){
   $jsonRequest = strpos($_SERVER['HTTP_ACCEPT'], 'json') !== false;
   
   $html    = vuelco($var, $jsonRequest);
   $callers = debug_backtrace();
   $trace   = false;

   foreach($callers as $call) {
      $trace[] = $call['class'] . '->' . $call['function'];
   }


   if ($jsonRequest) {
      array_push($html, array('trace' => $trace));

      if ($return)
           return json_encode($html);
      else echo   json_encode($html);
   }
   else TemplateHandler::getInstance()->setDump($html, $trace);
}

function vuelco($var, $jsonRequest=false){
   $aux  = (array) $var;

   $var_type = gettype($var);

   if ($var_type == 'object') {
      $var_data = $var_type.'#'.get_class($var);
   }
   elseif ($var_type == 'array') {
      $var_data = $var_type.' ('.sizeof($var).')';
   }
   else {
      $var_data = $var_type;
   }

   if (func_num_args() > 2)
        $txtvar = func_get_arg(2).' - '.$var_data;
   else $txtvar = $var_data;
   

   if ($jsonRequest)
         $data = array("datatype"=>$txtvar);
   else  $data = "\n<table class='dump' cellspacing=0 cellpadding=0>\n".
                 "<tr><td class='detail' colspan=2><i><font>$txtvar</font></i></td></tr>\n";

   foreach ($aux as $key => $value) {
      if ($jsonRequest) {
         $data['dataname']  = $key;
         $data['datavalue'] = '';
      }
      else {
         $data .= "<tr>\n";
         $data .= "<td class='cell-name'><b><font>".$key."</font></b></td>\n";
         $data .= "<td class='cell-value'><font>\n";   
      }

      if ((is_array ($value))||
          (is_object($value))){
         $xx    = $value;
         $data .= vuelco($xx, $jsonRequest, $key);
      }
      else {
         if ($jsonRequest)
              $data['datavalue'] = str_replace("\r\n",' ', str_replace("   ",' ',$value));
         else $data .= htmlspecialchars($value, ENT_QUOTES);
      }

      if (!$jsonRequest) {
         $data .= "</font></td>\n";
         $data .= "</tr>\n";
      }
   }

   if ($jsonRequest)
        $data  = $data;
   else $data .= "</table>\n";

   return $data;
}

function parsingTags($string) {
   $htmlTags[] = "img";
   $htmlTags[] = "input";
   $htmlTags[] = "textarea";
   $htmlTags[] = "span";
   $htmlTags[] = "br";
   $htmlTags[] = "div";
   $htmlTags[] = "label";
   $htmlTags[] = "i";
   $htmlTags[] = "strong";
   $htmlTags[] = "table";
   $htmlTags[] = "tr";
   $htmlTags[] = "td";
   $htmlTags[] = "th";
   $htmlTags[] = "head";
   $htmlTags[] = "title";

   foreach($htmlTags as $tag) {
      $string = preg_replace("/\<$tag(\w*)>/","&gt;$tag$1&lt;", $string);
   }
}
