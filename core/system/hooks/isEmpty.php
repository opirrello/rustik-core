<?php
function isEmpty($variable) {
   $isEmpty = false;

   if (is_array($variable)) {
      if (sizeof($variable) > 0) {
         foreach ($variable as $val) {
            if (!isEmpty($val)) {
               $isEmpty = false;
               break;
            }
            else $isEmpty = true;
         }
      }
      else $isEmpty = true;
   }
   else $isEmpty = empty($variable);

   return $isEmpty;
}

