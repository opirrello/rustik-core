<?php
class DataBaseHandler {
   protected $_engineName = '';
   protected $_requiredParams = '';
   
   public static function getInstanceOf($dbSchemaId=0) {
      global $g_dbEngineParams;

      $dbEngine   = (isEmpty($g_dbEngineParams[$dbSchemaId]['engine']))?DB_DEFAULTENG:$g_dbEngineParams[$dbSchemaId]['engine'];
      $dbHandler  = ucfirst($dbEngine) . 'Handler';
      $dbInstance = $dbHandler::getInstance($dbSchemaId);

      return $dbInstance;
   }

   protected function _getConfiguration($engineName = '') {
      global $g_dbEngineParams;

      $params = [];

      foreach($g_dbEngineParams as $engineId => $engineParams) {
         if ($engineParams['engine'] == $engineName) {
            $params = $g_dbEngineParams[$engineId];
            break;
         }
      }

      return $params;
   }

   protected function _validateConfiguration($engineName='') {
      global $g_dbEngineParams;

      $params = [];
      
      foreach($g_dbEngineParams as $engineId => $engineParams) {
         if ($engineParams['engine'] == $engineName) {
            $params = $g_dbEngineParams[$engineId];
            break;
         }
      }

      foreach($this->_requiredParams as $par) {
         if (!isset($params[$par])) {
            echo __CLASS__.": Parametro $par sin declarar";
            return false;
         }
         elseif (empty($params[$par])) {
            echo __CLASS__.": Parametro $par sin valor definido";
            return false;
         }
      }

      return true;
   }
}
