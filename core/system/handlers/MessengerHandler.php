<?php
class MessengerHandler {
   private $messages;
   public static $instance;

   private function __construct() {
      if (is_file(RF_CORE_CONFIGPATH .'messages.conf')) {
         include (RF_CORE_CONFIGPATH .'messages.conf'); //this include retrieve the array $url_mapping

         $this->__setMessages($messages);
      }
   }

   public static function getInstance() {
      if (!isset(self::$instance)) {
         self::$instance = new MessengerHandler();
      }

      return self::$instance;
   }

   private function __setMessages($messages) {
      $this->messages = $messages;
   }

   public function getDebugMessage($message_key, $parameters=FALSE, $type=RF_ERR_NORMAL, $language=FALSE) {
      $message    = $this->getMessage($message_key, $parameters, $type, $language);
      $trace      = debug_backtrace(FALSE);
      $curr_trace = $trace[1];
      $message    = "{$curr_trace['class']}::{$curr_trace['function']}() # ". $message;

      if ($type == RF_ERR_WARNING) {
         die($message);
      } 
      else {
         echo $message;
      }
   }

   public function getMessage($message_key, $parameters=FALSE, $language=FALSE) {
      $language = ($language)?$language:RF_LANG_DEFAULT;
      $message  = $this->messages[$language][$message_key];

      if (is_array($parameters)) {
         foreach ($parameters as $key=>$value) {
            $key++;
            $message = str_replace("%$key", $value, $message);
         }
      }
      else $message = str_replace("%1", $parameters, $message);

      return $message;
   }
}
