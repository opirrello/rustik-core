<?php
class SystemHandler {
   public static $instance;
   protected $_seedmin = 100;
   protected $_seedmax = 400;

   private function __construct() {
   }

   public static function getInstance() {
      if (!isset(self::$instance)) {
         self::$instance = new SystemHandler();
      }

      return self::$instance;
   }

   public function isCliRequest() {
      if (isset($_SERVER['argv']))
           return true;
      else return false;
   }

   public function isAjaxRequest() {
      if ((isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') ||
          (isset($_SERVER['HTTP_CUSTOM_REQUEST']) && strtolower($_SERVER['HTTP_CUSTOM_REQUEST']) == 'appservice'))
           return true;
      else return false;
   }

   public function isDefinedController($controllerName) {
      //$controllerName = strtolower($controllerName);
      //$controllerName = ucfirst($controllerName);

      //echo("<BR>".APP_CONTROLLERPATH ."{$controllerName}.class");
      if (is_file(RF_CORE_CONTROLLERPATH ."{$controllerName}.". RF_CORE_CLASSEXT) && class_exists($controllerName)) {
         return true;
      }
      else return false;
   }

   public function isDefinedHandler($entity) {
      $controller = ucfirst(strtolower($entity));

      if (is_file(RF_CORE_HANDLERPATH ."{$entity}Handler.". RF_CORE_CLASSEXT) && class_exists($controllerName)) {
         return true;
      }
      else return false;
   }

   public function isDefinedView($entity) {
      if (is_file(RF_CORE_VIEWPATH ."{$entity}.".RF_VIEW_FILETYPE)) {
         return true;
      }
      else return false;
   }

   public function loadController($controllerName) {
      if ($this->isDefinedController($controllerName)) {
         $controller = ucfirst(strtolower($controllerName));

         include_once (RF_CORE_CONTROLLERPATH . "{$controllerName}.". RF_CORE_CLASSEXT);
      }
      else die("No se puede cargar el controlador $controllerName. No esta definido");
   }

   public function loadHandler($handler) {
      if ($this->isDefinedHandler($handler)) {
         $handler = strtolower($handler);
         $handler = ucfirst($handler);

         include_once (RF_CORE_HANDLERPATH . "{$handler}Handler.class");
      }
      else die("No se puede cargar el handler $handler. No esta definido");
   }

   public function loadFunctions() {
      if ($handledir = opendir(RF_CORE_SYSHOOKPATH)) {
         while (false !== ($file = readdir($handledir))) {
            if ($file != '.' &&
                $file != '..' && 
                is_file(RF_CORE_SYSHOOKPATH.$file) &&
                substr(RF_CORE_SYSHOOKPATH.$file,-4) == '.'. RF_CORE_HOOKEXT) {

               include(RF_CORE_SYSHOOKPATH.$file);
            }
         }
      }
      closedir($handledir);

      if ($handledir = opendir(RF_CORE_APPHOOKPATH)) {
         while (false !== ($file = readdir($handledir))) {
            if ($file != '.' &&
                $file != '..' && 
                is_file(RF_CORE_APPHOOKPATH.$file) &&
                substr(RF_CORE_APPHOOKPATH.$file,-4) == '.'. RF_CORE_HOOKEXT) {

               include(RF_CORE_APPHOOKPATH.$file);
            }
         }
      }
      closedir($handledir);
   }

   public function setInitials() {
      global $init;

      if (!SystemHandler::getInstance()->isAjaxRequest()) {
         if (isset($init['js_files'])) {
            foreach ($init['js_files'] as $filename) {
               TemplateHandler::getInstance()->addJsFiles($filename);
            }
         }
   
         if (isset($init['css_files'])) {
            foreach ($init['css_files'] as $filename) {
               TemplateHandler::getInstance()->addCssFiles($filename);
            }
         }
      }
   }

   public function getConfig($confVar) {
   }

   public function isInstanceOf($classname) {
   }

   public function loadComposerClass($classname) {
      if (strpos('\\', $classname)) { 
            echo "<BR>ACA el namespace es $classspace";
         if (file_exists(RF_CORE_VENDORPSR4PATH . RF_CORE_VENDORPSR4FILE)) {
            $fp       = fopen(RF_CORE_VENDORPSR4PATH . RF_CORE_VENDORPSR4FILE);
            $psr4Conf = fread($fp);
            $psr4Arr  = json_decode($psr4Conf);

            dump($psr4Arr);
         }
      }
      else include_once $classname; 
   }

   static function autoload($classname) {
      if (strpos($classname, '\\') !== false) { 
         if (file_exists(RF_CORE_VENDORPSR4PATH . RF_CORE_VENDORPSR4FILE)) {
            $loader    = require RF_CORE_VENDORPSR4PATH . RF_CORE_VENDORPSR4FILE;
            $classfile = $loader->findFile($classname);
            include_once($classfile);
         }
      }
      else include_once ($classname . '.' . RF_CORE_CLASSEXT);
   }
}
