<?php
class MailHandler {
   public static $instance;
   public $to = array();
   public $html=false;
   public $debug=false;
   public $from;
   public $fromname;
   public $subject;
   public $body;
   public $attachments = array();
   private $host = RF_MAIL_HOST;
   private $port = RF_MAIL_PORT;
   private $username = RF_MAIL_USERNAME;
   private $password = RF_MAIL_PASSWORD;
   private $smtp_secure = RF_MAIL_SMTP_SECURE;

   private function __construct() {
      include RF_CORE_SYSLIBPATH."/phpmailer/class.phpmailer.php";
      //include RF_PHPMAILERPATH."class.phpmaileroauth.php";
      //include RF_PHPMAILERPATH."class.phpmaileroauthgoogle.php";
      include RF_CORE_SYSLIBPATH."/phpmailer/class.pop3.php";
      include RF_CORE_SYSLIBPATH."/phpmailer/class.smtp.php";
      //include RF_PHPMAILERPATH."get_oauth_token.php";

      $mailer = new PHPMailer;
      $this->_mailerObj = $mailer;
   }

   public function init() {
      $this->to = [];
      $this->html=false;
      $this->debug=false;
      $this->from=false;
      $this->fromname=false;
      $this->subject=false;
      $this->body=false;
      $this->attachments = [];
      
      $mailer = new PHPMailer;
      $this->_mailerObj = $mailer;
   }

   public static function getInstance() {
      if (!isset(self::$instance)) {
         self::$instance = new MailHandler();
      }

      return self::$instance;
   }

   public function setDebugTo($level) {
      $this->debug = $level;
   }

   public function isHTML($isHTML) {
      $this->html = $isHTML;
   }

   public function useTemplate($tplname, $parameters) {
      $tplObj = TemplateHandler::getInstance();

      // Setting custom snippetnames 
      if (is_array($parameters)) {
      foreach($parameters as $element => $content) {
         $hashPos = strpos($element, '#');

         if ($hashPos === false)
         continue;

         $tplObj->setSnippetAs( substr($element, $hashPos+1) , $content );
      }
      }

      //$tplObj->setParentObj($this);
      $tplObj->useStrictTemplate($tplname);

      // Setting custom variables and blocks
      if (is_array($parameters)) {
      foreach($parameters as $element => $content) {
         if (strpos('#', $element) !== false)
         continue;

         if (is_array($content)) {
            if ($tplObj->existsBlock($element) && !empty($content)) {
               foreach($content as $varname=>$value) {
                  $tplObj->setVariable($varname, $value);
                  $tplObj->appendBlock($element);
               }
            }
         }
         else {
            $tplObj->setVariable($element, $content);
         }
      }
      }

      $this->body = $tplObj->render('asResult');
      $this->html = true;
   }

   protected function _processMailerTagsIn($rTemplate) {
      preg_match_all('/\[\[MH:([\w:0-9]+)\]\]/', $rTemplate, $mailerProps);

      if (is_array($mailerProps) && isset($mailerProps[1]) && !empty($mailerProps[1])) {
         foreach ($mailerProps[1] as $prop) {

            $propStr = explode(":", $prop);
            $propCnt = false;

            $propName = $propStr[0];
            $propValue= $this->$propName;

            if (is_array($propValue)) {
               if (sizeof($propStr) > 1 && is_numeric($propStr[1]) && is_int($propStr[1]*1) )
                    $propCnt = $propValue[$propStr[1]];
               else $propCnt = implode(RF_MAIL_SEPARATOR, $propValue);
            }
            else {
               $propCnt = $propValue;
            }

            $rTemplate = str_replace("[[MH:$prop]]", $propCnt, $rTemplate);
         }
      }

      return $rTemplate;
   }

   public function send($options=null) {
      if (empty($this->to)) { die('No se ha definido ningun destinatario'); }

      if (is_null($options)) {
         if (defined('RF_MAIL_SMTP_OPTIONS')) {
            $options = json_decode(RF_MAIL_SMTP_OPTIONS, true);
         }

         $this->_mailerObj->SMTPOptions = $options;
      }

      $this->_mailerObj->CharSet = (empty($this->charset))?RF_MAIL_ENCODING:$this->charset;
      $this->_mailerObj->WordWrap = (empty($this->wordwrap))?RF_MAIL_WORDWRAP:$this->wordwrap;

      $this->_mailerObj->Host = (empty($this->host))?RF_MAIL_HOST:$this->host;
      $this->_mailerObj->Port = (empty($this->port))?RF_MAIL_PORT:$this->port;
      $this->_mailerObj->Username = (empty($this->username))?RF_MAIL_USERNAME:$this->username;
      $this->_mailerObj->Password = (empty($this->password))?RF_MAIL_PASSWORD:$this->password;

      if (RF_MAIL_USESMTP)
      $this->_mailerObj->IsSMTP();

      $this->_mailerObj->SMTPAuth = true;
      $this->_mailerObj->SMTPAutoTLS = false;
      $this->_mailerObj->SMTPSecure = (empty($this->smtp_secure))?RF_MAIL_SMTP_SECURE:$this->smtp_secure;

      /***** Setting debug level (1/2/3/4) **********/
      if ($this->debug) {
         $this->_mailerObj->SMTPDebug = $this->debug;
      }

      /***** Setting mail type (plain/html) **********/
      if ($this->html) {
         $this->_mailerObj->msgHTML($this->body, RF_SERVERROOT);
         $this->_mailerObj->isHTML(true);
      }
      else {
         $this->_mailerObj->Body = strip_tags($this->body);
         $this->_mailerObj->isHTML(false);
      }

      /***** Setting sender **********/
      if (empty($this->from))
           $this->_mailerObj->setFrom(RF_MAIL_FROM, RF_MAIL_FROMNAME);
      else $this->_mailerObj->setFrom($this->from, $this->fromname);

      /***** Setting destinations **********/
      foreach($this->to as $dest) {
         //---- find destination name
         $openTag = strpos($dest, '<');

         if ($openTag !== false) {
            if (substr($dest, -1) != '>') {
               die('El nombre de un destinatario esta mal declarado');
            }
            else {
               $destaddr = trim(substr($dest, 0, $openTag-1));
               $destname = trim(substr($dest, $openTag+1, -1));
            }
         }
         else {
            $destaddr = $dest;
            $destname = '';
         }

         //---- cheking if is common address or Bcc/Cc
         $openTag = strpos($dest, ':');

         if ($openTag !== false) {
            $addrType = strtolower(trim(substr($dest, 0, $openTag-1)));

            switch($addType) {
               case 'bcc':
                  $this->_mailerObj->addBCC($destaddr, $destname);
                  break;

               case 'cc':
                  $this->_mailerObj->addCC($destaddr, $destname);
                  break;
            }
         }
         else {
            $this->_mailerObj->addAddress($destaddr, $destname);
         }
      }

      /***** Setting subject **********/
      $this->_mailerObj->Subject = $this->subject;

      /***** adding attachments **********/
      foreach($this->attachments as $filename) {
         $this->_mailerObj->addAttachment($filename);
      }

      if (!$this->_mailerObj->send()) {
         if (RF_ENVIRONMENT == 'development' || RF_ENVIRONMENT == 'testing')
         MessengerHandler::getInstance()->getDebugMessage('mail-error-sending', $this->_mailerObj->ErrorInfo, RF_ERR_WARNING);

         logEvent("000 Mail Sending : FAIL ", $this->_mailerObj->ErrorInfo); 
         return false;
      }
      else {
         return true;
      }
   }

   public function attach($filepath) {
      $this->attachments[] = $filepath;
   }

   public function to() {
      $numSenders = func_num_args();

      for($i=0; $i<$numSenders; $i++) {
         $this->to[] = func_get_arg($i);
      }
   }

   public function from() {
      $numPars = func_num_args();

      $this->from = func_get_arg(0);

      if ($numPars > 1) {
         $this->fromname = func_get_arg(1);
      }
   }

   public function subject($text) {
      $this->subject = $text;
   }

   public function body($text) {
      $this->body = $text;
   }
}
