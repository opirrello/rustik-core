function httpRequest(method, url, data) {
   var callback = false;

   if (arguments.length >= 4) {
      if (typeof arguments[3] === 'function')
      afterSucess = arguments[3];
   }

   if (arguments.length == 5) {
      if (typeof arguments[4] === 'function')
      afterError = arguments[4];
   }

   $.ajax({  
      type: method,
      url: url,
      dataType:'json',
      data: data,
      beforeSend: function() { $('body').startSpin(); },
      success: function(response, textStatus, jqXHR) {
         if (response.redirect)
         window.location.href = response.redirect;

         if (typeof afterSucess === 'function')
         afterSucess(data, response);

         handleMessage('success', response);
         $('body').stopSpin();
      },
      error: function(jqXHR, textStatus) {
         if (jqXHR.responseJSON)
              var response = jqXHR.responseJSON;
         else var response = false;

         if (response.redirect)
         window.location.href = response.redirect;

         if (typeof afterError === 'function')
         afterError(data, response);

         handleMessage('error', response);
         $('body').stopSpin();
      }
   });
};
