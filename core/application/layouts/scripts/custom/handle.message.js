function handleMessage (stat, response) {
   if (response.message) {
      if (Array.isArray(response.message.description))
           var description = response.message.description.join('</p><p>');
      else var description = response.message.description;
 
      if (response.message.title)
           var title = response.message.title;
      else var title = false;

      if (response.confirmation)
           var confirmation = response.confirmation;
      else var confirmation = false;

      if (Array.isArray(response.message.verbose)) {
         var vbsArr = [];

         for (var i=0; i<response.message.verbose.length; i++) {
            var vbsObj  = response.message.verbose[i];
            var vbsType = (vbsObj.code != 200) ? 'danger':'success';

            vbsArr.push('<tr><td class="bg-'+vbsType+'-300">'+ vbsObj.label +'</td><td>'+ vbsObj.text +'</td></tr>');
         }

         var style = (vbsArr.length > 1) ? ' style="max-height:100px; height:100px" ':'';

         description += '<a href="#" class="view-verbose" data-action="collapse"><label>Mostrar detalles</label>&nbsp;<i class="caret"></i></a>'+
                        '<div class="verbose-container"'+style+'ss-container><table class="verbose">' + vbsArr.join('') + '</table></div>';
      }

      //--- create alert window with callback if was declared in 'after' property
      $('<div id="alert-window" data-title="'+title+'"><p>'+description+'</p></div>').confirmDialog({
         css:stat,
         buttonConfirm:{stat:'hide'},
         buttonCancel:{stat:'hide'}
      },function() {
         SimpleScrollbar.initAll();
      });
   }
}
