(function ($) {
    $.rsksearch = $.fn.rsksearch = function() {
       var options = {
          url: '',
          min_chars: 3,
          max_elements: 10
       };

       var withOpt = false;
       var callback= false;
       var target  = false;

       if (arguments.length > 3) return false;
       else if (arguments.length == 1) {
          if (typeof arguments[0] === 'object') {
             $.extend(options, arguments[0]);
          }
          else if (typeof arguments[0] === 'function') {
             callback = arguments[0];
          }

          target = $(this);
       }
       else if (arguments.length >= 2) {
          target = arguments[0];

          if (typeof arguments[1] === 'object') {
             $.extend(options, arguments[1]);

             if (arguments.length == 3) {
                if (arguments[2] === 'function')
                callback = arguments[2];
             }
          }
          else if (typeof arguments[1] === 'function') {
             callback = arguments[1];
          }
       }       

       target.on('keyup', function( event ) {
          var keycode = event.keyCode || event.which;
          
          if ( $.trim(target.val()).length && keycode !== 13 ) {
             event.preventDefault();

             if ($.trim(target.value).length && $.trim(search_object.value).length >= options.min_chars) {
                search(target.value);
             }
          }
       });
    }
});
