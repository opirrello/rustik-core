function postData(opt) {
   $.ajax({  
          type      : 'POST',
          url       : opt.url,
          dataType  : 'json',
          data      : opt.data,
          beforeSend: function() { $('body').startSpin(); },
          success   : opt.success,
          error     : opt.error
   });
}
