/*********************************************
  Confirm Dialog plugin (open dialog confirmation modal)
  Rustic framework plugin v1.0

  main initialization: line 1

*********************************************/
(function ($) {
   $.confirmDialog = $.fn.confirmDialog = function() {
      var $this   = this;
      var target  = false;
      var callback= false;
      var afterclose= false;
      var options = {
         open:true,
         title: 'Confirmar',
         css: '',
         width:450,
         position: {
            my: "center",
            at: "center",
            of: window
         },
         content: false,
         data: false,
         ajaxConfirm: {
            url: false,
            successTitle: 'Realizado',
            errorTitle: 'Fallido',
            successContent: '',
            errorContent: ''
         },
         buttonConfirm: {stat:'show', text:'Si', 'class':'confirm-button btn custom sucess'},         
         buttonCancel : {stat:'show', text:'No', 'class':'confirm-button btn custom default'},         
         callbackConfirm: false,
         callbackCancel : false
      };

      target = $(this);

      if (arguments.length == 1) {
         if (typeof arguments[0] === 'object') {
            $.extend(options, arguments[0]);
         }
         else if (typeof arguments[0] === 'function') {
            callback = arguments[0];
         }
      }
      else if (arguments.length == 2) {
         if (typeof arguments[0] === 'object') {
            $.extend(options, arguments[0]);

            if (typeof arguments[1] === 'function') {
               callback = arguments[1];
            }
            else {
               throw('passing more than one argument. Second of it must be a function');
            }
         }
         else {
            throw('passing more than one argument. First of it must be an object');
         }
      }       

      if (target.data('content')) {
         options.content = target.data('content');
      }
      if (target.data('title')) {
         options.title = target.data('title');
      }
      if (target.data('successTitle')) {
         options.ajaxConfirm.successTitle = target.data('successTitle');
      }
      if (target.data('errorTitle')) {
         options.ajaxConfirm.errorTitle = target.data('errorTitle');
      }
      if (target.data('successContent')) {
         options.ajaxConfirm.successContent = target.data('successContent');
      }
      if (target.data('errorContent')) {
         options.ajaxConfirm.errorContent = target.data('errorContent');
      }
      if (target.data('confirm')) {
         options.buttonConfirm.text = target.data('confirm');
      }
      if (target.data('cancel')) {
         options.buttonCancel.text = target.data('cancel');
      }

      if (!target.has('p').length) {
         target.append('<p></p>');
      }

      var btnConfirm = false;
      var btnCancel  = false;
      var buttonsArr = false;

      if (options.css != false && options.css.length) {
         options.css = ' '+options.css;
      }

      if (options.remoteContent) {
         var type = false;

         if (!options.remoteContent.hasOwnProperty('url')) {
            console.error('Error in Rustik Confirm Dialog plugin - Remote content url must be declared');
            return false;
         }
         if (!options.remoteContent.hasOwnProperty('callback')) {
            console.error('Error in Rustik Confirm Dialog plugin - Remote content callback action must be declared');
            return false;
         }
         if (options.remoteContent.hasOwnProperty('type')) {
            type = options.remoteContent.type;
         }

         if (options.remoteContent.hasOwnProperty('data')) {
            $.post(options.remoteContent.url, remoteContent.data, function(data) {
               options.remoteContent.callback(data);
            }, type);
         }
         else {
            $.get(options.remoteContent.url, function(data) {
               options.remoteContent.callback(data);
            }, type);
         }
      }

      if (options.buttonConfirm.stat == 'show') {
         if (!jQuery.isArray(buttonsArr)) {
            buttonsArr = [];
         }

         btnConfirm = {
            text: options.buttonConfirm.text, "class": options.buttonConfirm.class,
            click: function () {
               if (options.ajaxConfirm) {
                  if (!options.ajaxConfirm.hasOwnProperty('url')) {
                     console.error('Error in Rustik Confirm Dialog plugin - AJAX confirm url must be declared');
                     return false;
                  }

                  httpRequest('post', options.ajaxConfirm.url, options.data,
                     function(data, resp) {
                        target.parent().removeClass('error').addClass('success');

                        if (resp.message.title)
                             target.parent().find('.ui-dialog-title').html(resp.message.title);
                        else target.parent().find('.ui-dialog-title').html(options.ajaxConfirm.successTitle);

                        if (resp.message.description)
                             target.find('p').html(resp.message.description);
                        else target.find('p').html(options.ajaxConfirm.successContent);

                        target.parent().find('.ui-dialog-buttonpane').remove();

                        if (options.ajaxConfirm.success) { options.ajaxConfirm.success(resp); }
                        if (options.callbackConfirm) { options.callbackConfirm(); }
                        //else { $( this ).dialog( "close" ); }

                        $('body').stopSpin();
                     },
                     function(data, resp) {
                        target.parent().removeClass('success').addClass('error');
                        if (resp) {
                           if (resp.message.title)
                                target.parent().find('.ui-dialog-title').html(resp.message.title);
                           else target.parent().find('.ui-dialog-title').html(options.ajaxConfirm.errorTitle);

                           if (resp.message.description)
                                target.find('p').html(resp.message.description);
                           else target.find('p').html(options.ajaxConfirm.errorContent);
                        }
                        else {
                           target.parent().find('.ui-dialog-title').html(options.ajaxConfirm.errorTitle);
                           target.find('p').html(options.ajaxConfirm.errorContent);
                        }

                        target.parent().find('.ui-dialog-buttonpane').remove();

                        if (options.ajaxConfirm.error) { options.ajaxConfirm.error(); }
                        if (options.callbackConfirm) { options.callbackConfirm(); }
                        // else { target.dialog( "close" ); }
                                  
                        $('body').stopSpin();
                     }
                  );
               }
               else{
                  if (options.callbackConfirm) {
                     options.callbackConfirm();
                  }
                  else {
                     $( this ).dialog( "close" );
                  }
               }
            },
         };

         buttonsArr.push(btnConfirm);
      } 
      
      if (options.buttonCancel.stat == 'show') {
         if (!jQuery.isArray(buttonsArr)) {
            buttonsArr = [];
         }

         btnCancel = {
            text: options.buttonCancel.text, "class": options.buttonCancel.class,
            click: function () {
               if (options.callbackCancel) { options.callbackCancel(); }
               else { $( this ).dialog( "close" ); }
            }
         };

         buttonsArr.push(btnCancel);
      }

      var dialogObj = target.dialog({
               closeText: "",
               title: options.title,
               dialogClass: "confirmbox" + options.css,
               autoOpen: false,
               width: options.width,
               position: options.position,
               show: { effect: "fade", duration: 400 },
               hide: { effect: "fade", duration: 400 },
               modal: true,
               open: function(e, ui) {
                  if (target.parent().find('.ui-icon-closethick').length) {
                     if (!target.parent().find('.ui-icon-closethick').hasClass('fas fa-times')) {
                        target.parent().find('.ui-icon-closethick').addClass('fas fa-times');
                     }
                  }
                  else if (target.parent().find('button.ui-dialog-titlebar-close').length) {
                     target.parent().find('button.ui-dialog-titlebar-close').replaceWith(function() {
                        return $('<a href="#"></a>').addClass('ui-dialog-titlebar-close');
                     });

                     var closeButton = $('<a href="#"></a>').addClass('ui-dialog-titlebar-close');
                     var closeInner  = $('<span></span>').addClass('ui-icon ui-icon-closethick fas fa-times');

                     closeButton.append(closeInner);
                     closeButton.click(function(event) {
         					event.preventDefault();
                        $(e.target).dialog('close');
                     });

                     target.parent().find('.ui-dialog-titlebar').append(closeButton);
                     target.parent().find('button.ui-dialog-titlebar-close').remove();
                  }

                  if (options.content.length)
                  target.find('p').html(options.content);

                  if (options.sourceContent) {
                     options.sourceContent.callback(options.sourceContent.data);
                  }
                  else if (callback) callback();
               },
               close: function(e, ui) {
                  target.parent().removeClass('success').removeClass('error');
                  if (options.afterclose) options.afterclose();
               },
               buttons: buttonsArr
      });

      var putDanger = function() {
            target.removeClass('success').addClass('danger');
      };

      if (options.open)
      target.dialog('open');

      return dialogObj;
   }
})(jQuery);

