/*********************************************
  Listing plugin (listing table handler) 
  Rustic framework plugin v1.0

  main initialization: line 11

*********************************************/
(function ($) {
   $.fn.listing = $.fn.listing = function(opt) {
      var $this = this;
      var options = {
         container: false
      }

      if (!options.container) {
         console.error('Error in Rustik Listing plugin - container must be declared');
         return false;
      }

      /// HANDLING MASSIVE CHECK/UNCHECK CHECKBOX
      $('.filter.accchk').on('change', '#check-all', function() {
         $this.find('input.less').val('');

         if ($(this).is(':checked')) {
            $('.action span').removeClass('disabled');
            $('.action span').prop('disabled', false);
            //$('#filter-right-accdel span').removeClass('disabled');
            //$('#filter-right-acctrn select').prop('disabled', false);

            $this.find('.resultados input.checklist').val('all');
            $this.find('.resultados td.check input').prop('checked','checked');
         }
         else {
            $('.action span').addClass('disabled');
            $('.action span').prop('disabled', 'disabled');
            //$('#filter-right-accdel span').addClass('disabled');
            //$('#filter-right-acctrn select').prop('disabled', 'disabled');

            $this.find('.resultados input.checklist').val('');
            $this.find('.resultados td.check input').prop('checked',false);
         }
      });

      /// HANDLING INDIVIDUAL CHECK/UNCHECK CHECKBOX
      $('.resultados').on('change', '.check input:checkbox', function(e) {
         e.stopImmediatePropagation();

         var itemId = $(this).val();
         inoutCheckedItem(itemId);

         if (!$(this).is(':checked')) {
            $('#check-all').prop('checked', false);
         }
         else if ( container.find('.resultados input.checklist').val() == 'all' ) {
            $('#check-all').prop('checked', true);
         }
      });
   };
})(jQuery);




