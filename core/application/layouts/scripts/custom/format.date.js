function formatDate(orig_date) {
   /*
   $orig_format { dmY/Ymd/ymd}
    * */
   if (!orig_date) return orig_date;

   var format = false;
   var orig_format = false;
   var omit_time = false;

   if (arguments.length >= 1) {
      format = arguments[1];

      if (arguments.length >= 2) {
         orig_format = arguments[2];

         if (arguments.length == 3) {
            omit_time = arguments[3];
         }
      }
   }
      
   dateTimeStruct = orig_date.split(' ');
   dateStruct = dateTimeStruct[0];

   ori_formStruct = (orig_format) ? orig_format.split() : false;
   des_formStruct = (format)      ? format.split() : false;

   if (dateStruct.indexOf('-') != -1) {
      dateStruct = dateStruct.split('-');

      if (!ori_formStruct) {
         ori_formStruct = new Array('y','m','d','-');
      }
      if (!des_formStruct) {
         des_formStruct = new Array('d','m','y','/');
      }
   }
   else if(dateStruct.indexOf('/') != -1) {
      dateStruct = dateStruct.split('/');

      if (!ori_formStruct) {
         ori_formStruct = new Array('d','m','y','/');
      }
      if (!des_formStruct) {
         des_formStruct = new Array('y','m','d','-');
      }
   }

   var aux = [];

   for (var ori_pos = 0; ori_pos<ori_formStruct.length; ori_pos++) {
      var ori_elem = ori_formStruct[ori_pos];
      for (var des_pos = 0; des_pos<des_formStruct.length; des_pos++) {
         var des_elem = des_formStruct[des_pos];

         if (ori_elem == des_elem) {
            aux[des_pos] = dateStruct[ori_pos];
         }
      }
   }

   if (aux.length == 4)
   aux.pop();

   var new_date = aux.join(des_formStruct[3]);
   dateTimeStruct[0] = new_date;

   if (omit_time)
        final_date = dateTimeStruct[0];
   else final_date = dateTimeStruct.join(' ');

   return final_date;
}
