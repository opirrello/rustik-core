(function ($) {
   $(document).ready(function() {
      logoutUser = function() {
         $.ajax({
            type: 'post',
            url: '/auth/logout',
            data:{},
            beforeSend: function() { $('body').startSpin(); },
            success: function (response) {
               window.location.href = '/auth/sign_in';
            }
         });

         return false;
      }

      $('#user-profile').click(function(e) {
         e.preventDefault();
         window.location.href="/users/edit/profile";
      });

      $('#user-logout, #user-profile-logout').click(function(e) {
         e.preventDefault();

         logoutUser();
         /*
         $.ajax({
            type: 'post',
            url: '/auth/logout',
            data:{},
            beforeSend: function() { $('body').startSpin(); },
            success: function (response) {
               window.location.href = '/auth/sign_in';
            }
         });*/

         return false;
      });

      $('#cotizar').click(function(e) {
         window.location.href = '/cotizacion/cotizar';
      });

      $('#administracion').click(function(e) {
         window.location.href = '/administracion';
      });
   });
})(jQuery);
