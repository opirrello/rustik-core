/*********************************************
  roundTo prototype method
  Rustic framework plugin v1.0

  main initialization: line 363

*********************************************/

Number.prototype.roundTo = function(decimals) {
      if (!this || this == '') {
         console.error('Error in Rustik roundTo plugin - a number value must be provided');
         return false;
      }

      var str = this.toString();

      if (isNaN(decimals)) {
         console.error('Error in Rustik roundTo plugin - decimal scale must be an integer number');
         return false;
      }
      if (isNaN(str)) {
         console.error('Error in Rustik roundTo plugin - a number value must be provided');
         return false;
      }

      var arr = str.split('.');

      // control if there isn't any decimal number, or decimal length
      // is less than decimals required ("decimals" parameter)
      if (arr.length == 1 || arr[1].length <= decimals) {
         var newVal = parseFloat(str);
         var auxStr = str;
      }
      else {
         var needed = arr[1].substring(0,decimals); //--- taken N first decimals needed ("decimals" parameter) 
         var restof = arr[1].substring(decimals);   //--- taken rest of decimals until the end
         var decArr = restof.split('');             //--- rest of decimals placed into a list of values
         var auxArr = []; //new Array();
         var stopit = false;
    
         
         // walk across rest of decimal values until one of them implies an increment
         // in the previous value walked (and increment this last value and go away)
         // the auxArr store each lowest value.
      
         for (i in decArr){
            var digito = parseInt(decArr[i]);
             
            if (digito > 4) {
               if (auxArr.length)
                    auxArr[auxArr.length-1]++;
               else auxArr.push(digito);

               break;
            }
            else {
               auxArr.push(digito);
            }
         }

         needed = parseInt(needed);

         if (decArr[0] > 4)
         needed++;

         var auxStr = arr[0]+'.'+needed;
         var newVal = parseFloat(auxStr);
      }

      var restype= (arguments.length > 1) ? arguments[1]:'number'; 

      switch (restype) {
         case 'number': return newVal; break;
         case 'string': return auxStr; break;
         default      : console.error('Error in Rustik roundTo plugin - result type invalid ("number" or "string" accepted)');
      }
}

