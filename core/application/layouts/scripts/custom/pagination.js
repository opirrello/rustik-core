/*********************************************
  Pagination plugin
  Rustic framework plugin v1.0

  main initialization: line 11

*********************************************/
(function ($) {
   $.fn.pagination = $.fn.pagination = function(opt) {
      var $this = this;
      var options = {
         url: false,
         spin: false,
         pageLayout: false,
         itemBuilder: false,
         container: false,
         afterPaging: false,
         emptyListMessage: 'without data to list',
         checklistName: 'listdata',
         beforePaging: false,
         offStyle: 'off',
         getFiltering: function() { return false },
         queryElement: false,
         orderElement: false,
         orientElement: false,
         orderByClass: 'selected',
         currPageClass:'current',
         callback: false
      };

      $.extend(options, opt);

      var currClass = $(this).attr('class');

      if (!options.container) {
         console.error('Error in Rustik Pagination plugin - container must be declared');
         return false;
      }

      var list_container = this;
      var searchElt = (options.queryElement) ? options.queryElement : this.find('.'+currClass+'-search');
      //var headerElt = this.find('.'+currClass+'-controls');
      var orienElt  = (options.orientElement) ? options.orientElement : this.find('.orientation');
      var ordbyElt  = (options.orderElement) ? options.orderElement : false;
      var listElt   = (options.listElement) ? options.listElement : this.find('.'+currClass+'-list');
      var pagesElt  = this.find('.'+currClass+'-pages');

      if (!pagesElt.length) {
         console.error('Error in Rustik Pagination plugin - pages container ".'+currClass+'-pages" does not exist');
         return false;
      }
   
      //----- function to add/remove item from checklist
      var inoutCheckedItem = function(entId) {
         var allChecked = $this.find('input.checklist').val() == 'all';
         var totalRows  = ($('.resultados-pages').length) ?
                           $('.resultados-pages').attr('rel').split('::')[0].split(':')[1] :
                           $('.datatable-header').attr('rel');
   
         if (allChecked) {
            var entCont = $this.find('input.less');
            var entValue= $this.find('input.less').val();
         }
         else {
            var entCont = $this.find('input.checklist');
            var entValue= $this.find('input.checklist').val();
         }
   
         if (!entValue)
              var entList = [];
         else var entList = entValue.split(':');
   
         if (entList.indexOf(entId) == -1)
              entList.push(entId);
         else entList.splice(entList.indexOf(entId), 1);
  
         entCont.val(entList.join(':'));
   
         //---------- control gral check & list of selected/excluded ------------------------
         //all checked and some items are excluded
         if (allChecked && entValue) { 
            if (entList.length == totalRows) { ///if the excluded items are equal to total rows...
               $('#check-all').prop('checked', false);                           // unckeck all
               $this.find('input.checklist').val('');  //clean selected
               $this.find('input.less').val('');       //clean excluded
            }
         }
   
         //---------- control gral check to disable/enable gral action buttons --------------
         if (!$this.find('input.checklist').val()) {
            $('.action *').addClass('disabled');
            $('.action *').prop('disabled', true);
         }
         else {
            $('.action *').removeClass('disabled');
            $('.action *').prop('disabled', false);
         }
      }

      /// HANDLING MASSIVE CHECK/UNCHECK CHECKBOX
      $this.find('.filter.accchk').on('change', '#check-all', function() {
         $this.find('input.less').val('');

         if ($(this).is(':checked')) {
            $this.find('.action *').removeClass('disabled');
            $this.find('.action *').prop('disabled', false);
            //$('#filter-right-accdel span').removeClass('disabled');
            //$('#filter-right-acctrn select').prop('disabled', false);

            $this.find('input.checklist').val('all');
            $this.find('td.check input').prop('checked','checked');
         }
         else {
            $('.action *').addClass('disabled');
            $('.action *').prop('disabled', 'disabled');
            //$('#filter-right-accdel span').addClass('disabled');
            //$('#filter-right-acctrn select').prop('disabled', 'disabled');

            $this.find('input.checklist').val('');
            $this.find('td.check input').prop('checked',false);
         }
      });

      /// HANDLING INDIVIDUAL CHECK/UNCHECK CHECKBOX
      $this.on('change', '.check input:checkbox', function(e) {
         e.stopImmediatePropagation();

         var itemId = $(this).val();
         inoutCheckedItem(itemId);

         if (!$(this).is(':checked')) {
            $('#check-all').prop('checked', false);
         }
         else if ( $this.find('input.checklist').val() == 'all' ) {
            $('#check-all').prop('checked', true);
         }
      });


      /// defining orderby elements and handling method
      if (ordbyElt.length) {
         var eltObj  = ordbyElt;
         var eltType = eltObj.length ? eltObj.get(0).tagName : '';
      }
      else {
         var eltObj  = list_container.find('.orderby');
         var eltType = eltObj.length ? eltObj.get(0).tagName : '';
         //var lstType = listElt.get(0).tagName;
      }

      if (eltType == 'SELECT') {
         this.find('.orderby').change(function() {
            handleOrderBy(eltObj.find(':selected'));
         });
      }
      else if (eltType == 'INPUT' && eltObj.attr('type') == 'radio') {
         this.find('.orderby').change(function() {
            handleOrderBy(list_container.find('.orderby:checked'));
         });
      }
      else {
         this.find('.ord').click(function() {
            handleOrderBy($(this));
         });
      }

      handleOrderBy = function(el) {
         if (orienElt.length && el.hasClass(options.orderByClass)) {
            return;
         }

         /// if orientation is handled in other element...
         if (orienElt.length) {
            var orderby = getOrderBy();
            var orient = orienElt.find('.' + options.orderByClass).attr('rel');
         }
         else {
            var ordori = el.attr('rel').split(':');
            var orderby = ordori[0];
            var orient = ordori[1];
         }

         orient = (orient == 'asc') ? "desc":"asc";

         $('.ord.'+options.orderByClass).removeClass(options.orderByClass);
         el.addClass(options.orderByClass);

         var page = pagesElt.find('.page.'+options.currPageClass).text();

         getResults(page, orderby, orient);
      }

      var getOrderBy = function(el) {
         var eltValue= false;

         if (eltType == 'SELECT') {
            var eltValue = eltObj.find(':selected').val();
         }
         else if (eltType == 'INPUT' && eltObj.attr('type') == 'radio') {
            var eltValue = list_container.find('.orderby:checked').val();
         }
         else if (eltType == 'LI') {
            var eltValue = list_container.find('.ord.'+options.orderByClass).attr('rel');
         }
         else var eltValue = eltObj.text();

         return eltValue;
      }

      function getResults(page, orderby, orient) {
         var filters = options.getFiltering();
         var query   = false;

         if (searchElt.find('.query').length) {
            var queryObj = searchElt.find('.query');
            var queryTyp = queryObj.get(0).tagName;

            if (queryTyp == 'INPUT')
                 query = queryObj.val();
            else query = queryObj.text();
         }

         if (!query)
         query = '';

         if (arguments.length > 3)
              var callback = arguments[2];
         else var callback = false;

         $.ajax({
            type: 'post',
            dataType: 'json',
            url:options.url,
            data:{query:query, orderby:orderby, orient:orient, page:page, filters:filters, get_as:'json'},
            beforeSend: function() { $('body').startSpin(options.spin); },
            success: function(response) {
               /// pre callback
               if (options.beforePaging) {
                  options.beforePaging(response);
               }

               if (response.results && response.results.length) {
                  var list = listElt.find('rows-container');
                  var filt = (response.filtering !== null);

                  if (!list.length) 
                  list = listElt;

                  /// updating column ordering data
                  for (var colname in response.columnset) {
                     if (response.columnset[colname].orderby) {
                        $('#'+response.columnset[colname].code).attr('rel', response.columnset[colname].code+':'+response.columnset[colname].orient);
                        $('#'+response.columnset[colname].code).removeClass(response.columnset[colname].orientp);
                        $('#'+response.columnset[colname].code).addClass(response.ordering.orient.toLowerCase());
                     }
                  }

                  /// cleaning current list and rendering new results
                  list.empty();

                  for (var i in response.results) {
                     if (options.itemBuilder) 
                          var it = options.itemBuilder(response.results[i], filt, response);
                     else var it = itemBuilder(response.results[i], filt, response);
                     list.append(it);
                  }
               }

               /// setting footer text & remove exceeds pages if last page-group has lower page num
               var pagesList = options.container.find('.resultados-pages span a');

               if (response.pagination) {
                  /// if current num pages recovered in async mode is lower than initial number, then remove excedent
                  if (pagesList.length > response.pagination.num_pages)
                  pagesList.slice(response.pagination.num_pages).remove();

                  if (response.pagingdet) 
                  options.container.find('.dataTables_info').text(response.pagingdet);

                  if (response.extra_info)
                  extra_info = response.extra_info;

                  options.container.parent().find('.datatable-scroll').show();
                  options.container.parent().find('.datatable-footer').show();
                  options.container.parent().find('.sin-resultados').remove();
               }
               else {
                  options.container.parent().find('.datatable-scroll').hide();
                  options.container.parent().find('.datatable-footer').hide();
      
                  if (!options.container.parent().find('.sin-resultados').length) {
                     options.container.parent().find('.datatable-scroll').parent().append(
                       '<div class="sin-resultados alert alert-danger">'+
                       '<strong>'+ options.emptyListMessage +'</strong>'+
                       '</div>'
                     );
                  }
               }

               /// post callback
               if (options.afterPaging) {
                  options.afterPaging(response);
               }

               if (response.pagination)
               encodePgnData(response.pagination);

               resetShortcuts(page);

               $('body').stopSpin();
            },
            error: function(xhr, stat, error) { $('body').stopSpin(); }
         });
      }

      function encodePgnData(arrData) {
         var pgSerial = 'nr:' + arrData.num_rows + '::' +
                        'np:' + arrData.num_pages + '::' +
                        'rpp:'+ arrData.rows_per_page + '::' +
                        'ppg:'+ arrData.pages_per_group + '::' +
                        'cp:' + arrData.curr_page + '::' +
                        'fp:' + arrData.first_page;

         pagesElt.attr('rel',pgSerial);
      }

      function parsePgnData(strData) {
         var dataArr = strData.split('::');
         var dataObj = {};

         for(var i=0; i<dataArr.length; i++) {
            var itemArr = dataArr[i].split(':');
            dataObj[itemArr[0]] = itemArr[1];
         }

         return dataObj;
      }

      function resetShortcuts(page) {
         if (!pagesElt.length)
         return false;

         var nextBtn = pagesElt.find('.shortcut.next');
         var lastBtn = pagesElt.find('.shortcut.last');
         var firstBtn= pagesElt.find('.shortcut.first');
         var prevBtn = pagesElt.find('.shortcut.prev');

         var pgdata = parsePgnData(pagesElt.attr('rel'));
         var currp  = pagesElt.find('.page.'+options.currPageClass).text();
         var currg  = Math.ceil(currp / pgdata.ppg); 

         var gotop  = page;
         var gotog  = Math.ceil(gotop / pgdata.ppg);
         var cantg  = Math.ceil(pgdata.np / pgdata.ppg);
         
         ////// try to identify current page container (li, a, label, etc.)
         //var pagLayout = pagesElt.find('.page').get(0).tagName.toLowerCase();
         var pagClone  = pagesElt.find('.page').first().clone().removeClass(options.currPageClass);
         var pagGroup  = pagesElt.find('.page');
         var pagParent = pagGroup.parent();

         /// changing group of pages (new values)
         var indic = gotog * pgdata.ppg;
         var limit = (indic > pgdata.np) ? pgdata.ppg - (indic - pgdata.np) : pgdata.ppg;

         /// cleaning current page container
         pagGroup.remove();

         for (var i=0; i < limit; i++) {
            var pag = (pgdata.ppg * gotog) - (pgdata.ppg - 1) + i;
            var act = (pag == gotop) ? options.currPageClass:'';
            var obj = pagClone.clone();
            
            obj.text(pag);
            obj.attr('rel', pag);
            obj.addClass(act);

            pagParent.append(obj);
         }

         /// setting first/prev buttons
         if (gotog != 1) {
            firstBtn.attr('rel', pgdata.fp);
            prevBtn.attr ('rel', (gotog - 2) * pgdata.ppg + 1);
            firstBtn.removeClass(options.offStyle);
            prevBtn.removeClass(options.offStyle);
         }
         else {
            firstBtn.attr('rel','');
            prevBtn.attr ('rel','');
            firstBtn.addClass(options.offStyle);
            prevBtn.addClass(options.offStyle);
         }

         /// setting next/last buttons
         if (gotog != cantg) {
            lastBtn.attr('rel', pgdata.np);
            nextBtn.attr('rel', (gotog * pgdata.ppg) + 1);
            lastBtn.removeClass(options.offStyle);
            nextBtn.removeClass(options.offStyle);
         }
         else {
            lastBtn.attr('rel','');
            nextBtn.attr ('rel','');
            lastBtn.addClass(options.offStyle);
            nextBtn.addClass(options.offStyle);
         }
      }

      //// Get results by page
      ////only handle shortcut without 'disabled' style
      pagesElt.find('.shortcut').click(function() {
         if ($(this).hasClass(options.offStyle))
         return false;

         if (orienElt.length) {
            var orient = orienElt.find('.' + options.orderByClass).attr('rel');
            var orderby = getOrderBy();
         }
         else {
            var ordori = $('.ord.'+options.orderByClass).attr('rel').split(':');
            var orderby = ordori[0];
            var orient = ordori[1];
         }

         getResults($(this).attr('rel'), orderby, orient);
      });
      
      //// Get results by page
      pagesElt.on('click','.page',function() {
         if (!$(this).hasClass(options.currPageClass)) {

            if (orienElt.length) {
               var orient = orienElt.find('.' + options.orderByClass).attr('rel');
               var orderby = getOrderBy();
            }
            else {
               if ($('.ord.'+options.orderByClass).length) 
                    var ordori = $('.ord.'+options.orderByClass).attr('rel').split(':');
               else var ordori = $('.ord').first().attr('rel').split(':');

               var orderby = ordori[0];
               var orient = ordori[1];
            }

            var query  = searchElt.find('.query').text();
            var filters= options.getFiltering();
            var curr   = pagesElt.find('.page.'+options.currPageClass).text();
            var page   = $(this).text();

            pagesElt.find('.page.'+options.currPageClass).removeClass(options.currPageClass);
            $(this).addClass(options.currPageClass);

            getResults(page, orderby, orient);
         }
      });

      //// Get results by orientation
      orienElt.find('li').click(function() {
         if (!$(this).hasClass(options.orderByClass)) {
            var orient = $(this).attr('rel');
            var orderby= getOrderBy();
            var page   = pagesElt.find('.page.'+options.currPageClass).text();

            $('.orientation li').removeClass(options.orderByClass);
            $(this).addClass(options.orderByClass);

            getResults(page, orderby, orient);
         }
      });

      var afterPaging = function(response) {
         /*
         var pagesList = $('.resultados-pages span a');

         if (response.pagination) {
            /// if current num pages recovered in async mode is lower than initial number, then remove excedent
            if (pagesList.length > response.pagination.num_pages)
            pagesList.slice(response.pagination.num_pages).remove();

            if (response.pagingdet) 
            $('.dataTables_info').text(response.pagingdet);

            if (response.extra_info)
            extra_info = response.extra_info;

            $('.datatable-scroll').show();
            $('.datatable-footer').show();
            $('.sin-resultados').remove();

            if (options.callback) {
               options.callback();
            }
         }
         else {
            $('.datatable-scroll').hide();
            $('.datatable-footer').hide();

            if (!$('.sin-resultados').length) {
               $('.datatable-scroll').parent().append(
                 '<div class="sin-resultados alert alert-danger">'+
                 '<strong>'+ options.emptyListMessage +'</strong>'+
                 '</div>'
               );
            }
         }*/

         return false;
      };

      var setCheckStatus = function(cotId) {
         var stat = false;
         var esta = false;
         var allChecked = options.container.find('.resultados input.checklist').val() == 'all';

         if (allChecked) {
            var cotCont = options.container.find('.resultados input.less');
            var cotValue= options.container.find('.resultados input.less').val();
         }
         else {
            var cotCont = options.container.find('.resultados input.checklist');
            var cotValue= options.container.find('.resultados input.checklist').val();
         }

         if (!cotValue)
              var cotList = [];
         else var cotList = cotValue.split(':');

         for (var idx in cotList) {
            if (cotId == cotList[idx]) {
               stat = (allChecked) ? false : true;
               esta = true;
               break;
            }
         }

         if (esta)
              return stat;
         else return (allChecked) ? true : false;
      };

      var itemBuilder = function(itemData, with_filters, response) {
         var itemCss= (itemData.row_css !== false) ? ' '+itemData.row_css : '';
         var itemTr = document.createElement('tr');
         itemTr.setAttribute('class', itemData.row_odd+itemCss);

         if (itemData.row_props !== false) {
            for(var prop in itemData.row_props) {
               itemTr.setAttribute(prop, itemData.row_props[prop]);
            }
         }

         /*********** creating td for checkbox ************/
         var rowCell = document.createElement('td');
         rowCell.setAttribute('class','check');
   
         var check  = document.createElement('input');
         check.setAttribute('type', 'checkbox');
         check.setAttribute('name', options.checklistName+'[]');
         check.setAttribute('value', itemData.id);
   
         if ( setCheckStatus(itemData.id) )
         check.setAttribute('checked','checked');
   
         rowCell.appendChild(check);
         itemTr.appendChild(rowCell);
   
         /*********** creating td group for data ****************/
         createCellUsingInto(itemData, itemTr, response.columnset);
   
         /*********** creating span for actions ************/
         var rowCell = document.createElement('td');
         var accUl   = document.createElement('ul');
   
         rowCell.setAttribute('class','text-center');
         accUl.setAttribute('class','icons-list resultados-actions');
   
         for (action in itemData.actions) {
            var accObj= itemData.actions[action];
            var accLi = document.createElement('li');
            var accLn = document.createElement('a');
            var accI  = document.createElement('i');
   
            accLn.setAttribute('title', accObj.desc);
            accLn.setAttribute('alt'  , accObj.desc);
            accLn.setAttribute('class', accObj.name + accObj.omit);
            accLn.setAttribute('rel'  , itemData.id);
            accI.setAttribute('class' , 'fa fa-'+accObj.css);
   
            if (accObj.extra) {
               for (attr in accObj.extra) {
                   accLn.setAttribute(attr, accObj.extra[attr]);
               }
            }
   
            accLn.appendChild(accI);
            accLi.appendChild(accLn);
            accUl.appendChild(accLi);
         }
   
         rowCell.appendChild(accUl);
         itemTr.appendChild(rowCell);
             
         return itemTr;
      };

      var createCellUsingInto = function(cellData, rowObj, columnset) {
         var dataCols = Object.keys(columnset);
   
         for (idx in dataCols) {
            for (col in cellData) {
               if (col == dataCols[idx]) {
                  var rowCell = document.createElement('td');
                  rowCell.setAttribute('class','sorting_1');
  
                  cellData[col] = (cellData[col] == null) ? '' : cellData[col];
 
                  if (cellData[col] != null && cellData[col].html)
                       rowCell.appendChild(document.createRange().createContextualFragment(cellData[col].html));
                  else rowCell.appendChild(document.createTextNode(cellData[col]));

                  rowObj.appendChild(rowCell);

                  break;
               }
            }
         }
      };

      $this.paginate = function(page) {
         if (orienElt.length) {
            var orient = orienElt.find('.' + options.orderByClass).attr('rel');
            var orderby = getOrderBy();
         }
         else {
            if ($('.ord.'+options.orderByClass).length) {
               var ordori = $('.ord.'+options.orderByClass).attr('rel').split(':');
               var orderby = ordori[0];
               var orient = ordori[1];
            }
            else console.error('Error in Rustik Pagination plugin - There are not any orderby column defined');
         }

         pagesElt.find('.page').each(function() {
            if (!$(this).hasClass(options.currPageClass)) {
               if (options.pageLayout) {
                  //-- used to identify html object if some layout for page is defined
                  var auxHtml = $(options.pageLayout);
                  var auxType = auxHtml.get(0).tagName.toLowerCase();
                  var seen    = $(this).find(auxType).text();
               }
               else {
                  var seen = $(this).text();
               }

               var curr = pagesElt.find('.page.'+options.currPageClass).text();

               if (seen == page) {
                  pagesElt.find('.page.'+options.currPageClass).text('');
                  pagesElt.find('.page.'+options.currPageClass).append( ((options.pageLayout)?options.pageLayout.replace('%1',page):page) );
                  pagesElt.find('.page.'+options.currPageClass).removeClass(options.currPageClass);

                  $(this).html(page);
                  $(this).addClass(options.currPageClass);
               }
            }
         });

         if (arguments.length > 1)
              getResults(page, orderby, orient, arguments[1]);
         else getResults(page, orderby, orient);
      };

      return $this;
   }
})(jQuery);
