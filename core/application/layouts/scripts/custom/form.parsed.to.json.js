$.formParseToJson = $.fn.formParseToJson = function() {
   var formdata = this.serializeArray();

   var formObj  = {};

   for (var i in formdata) {
      var elem = formdata[i];

      if (elem['name'].indexOf('[]') != -1) {
         var elemName = elem['name'].replace('[]','');

         if (!formObj.hasOwnProperty(elemName)) {
            formObj[elemName] = [];
         }

         formObj[elemName].push(elem['value']);
      }
      else formObj[elem['name']] = elem['value'];
   }

   return formObj;
}
