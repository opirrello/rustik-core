(function ($) {
   $(document).ready(function() {
      $('#put-left').click(function(){
        $('#alin').css({'float':'left'});
      });

      $('#put-right').click(function(){
        $('#alin').css({'float':'right'});
      });

      $('#get-info').click(function(){
        $('#info').toggle();
      });

      $('#show-hide').click(function(e){
        if ($('#content').is(':visible')){
           $('#content').hide();
           $(this).text('+');
        } 
        else{
           $('#content').show();
           $(this).text('-');
        }
      });
    });
})(jQuery);
