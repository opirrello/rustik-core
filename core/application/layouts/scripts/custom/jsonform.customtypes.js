(function ($) {
   JSONForm.fieldTypes['custom'] = {
      'template': '<div' + 
                  '<%= (elt.htmlId ? " id=\'" +  elt.htmlId + "\' " : "") %>' +
                  '<%= (elt.htmlClass ? " class=\'" +  elt.htmlClass + "\' " : "") %>'+
                  '>' +
                  '<% if (node.title && !elt.notitle) { %><label class="control-label" for="<%= node.id %>"><%= node.title %></label><% } %>' +
                  '<%= content %>'+
                  '</div>',
      'onBeforeRender': function (data, node) {
         var render = function(elm) {
            var html = '';

            _.each(elm.inner, function (el, idx) {
               var elem = '';
               var subelem = '';

               if (el.inner) {
                  var subelem = render(el);
               }
    
               elem = "<"+el.type;

               if (el.params) {
                  _.each(el.params, function (par, idx) {
                     elem = elem +" "+ idx +"=\""+ par + "\"";
                  });
               }

               elem = elem + ">" + subelem;
   
               if (el.closed && el.textNode) {
                  elem = elem + el.textNode + '</'+el.type+'>';
               }
               else if (el.closed) elem = elem + '</'+el.type+'>';
   
               html = html + elem;
            });
         
            return html;
         }

         var elt = node.formElement || {};
         data.content = '';

         if (elt.inner)
              data.content = render(elt);
         else data.content = '';
      }
   };

   JSONForm.fieldTypes['imageupload'] = {
      'template': '<div class="form-group jsonform-error-<%=node.name%> imageupload<%= node.schemaElement.required ? \' jsonform-required\':\'\'%>">'+
                  '<label class="control-label"><%=node.title%></label>'+
                  '<div class="controls" rel="<%=node.required%>">'+
                  '<div id="<%=node.id%>" class="fileupload">'+
                  '<input type="text" name="<%=node.name%>" id="control-<%=node.name%>" style="display:none" value="" />'+
                  '<div class="box has-advanced-upload">'+
                  '<div class="box-thumb">'+
                     '<ul></ul>'+
                     '<span>'+
                       '<label></label>'+
                       '<a id="add-more" href="#" alt="<%=more_label%>" title="<%=more_label%>"></a>'+
                       '<a id="empty-list" href="#" alt="<%=empty_label%>" title="<%=empty_label%>"></a>'+
                     '</span>'+
                  '</div>'+
                  '<div class="box-input">'+
                     '<svg class="icon" xmlns="http://www.w3.org/2000/svg" width="50" height="43" viewBox="0 0 50 43">'+
                     '<path d="M48.4 26.5c-.9 0-1.7.7-1.7 1.7v11.6h-43.3v-11.6c0-.9-.7-1.7-1.7-1.7s-1.7.7-1.7 1.7v13.2c0 .9.7 1.7 1.7 1.7h46.7c.9 0 1.7-.7 1.7-1.7v-13.2c0-1-.7-1.7-1.7-1.7zm-24.5 6.1c.3.3.8.5 1.2.5.4 0 .9-.2 1.2-.5l10-11.6c.7-.7.7-1.7 0-2.4s-1.7-.7-2.4 0l-7.1 8.3v-25.3c0-.9-.7-1.7-1.7-1.7s-1.7.7-1.7 1.7v25.3l-7.1-8.3c-.7-.7-1.7-.7-2.4 0s-.7 1.7 0 2.4l10 11.6z"></path>'+
                     '</svg>'+
                     '<input class="file" type="file" name="<%=node.name%>[]" id="fileupload-<%=node.name%>" data-multiple-caption="{count} files selected" multiple />'+
                     '<label for="fileupload-<%=node.name%>"><strong><%=choose_label%></strong> <span class="dragndrop"><%=drag_label%></span></label>'+
                     '<% if (upload) { %>'+
                     '<button class="button" type="submit"><%=upload_label%></button>'+
                     '<% } %>'+
                  '</div>'+
                  '</div>'+
                  '</div>'+
                  '</div>'+
                  '</div>',
      'fieldtemplate': false,
      'inputfield': true,
      'onBeforeRender': function (data, node) {
         data.choose_label = (!node.formElement.config.titles.choose)? 'Choose a file' : node.formElement.config.titles.choose;
         data.drag_label   = (!node.formElement.config.titles.drag)? 'or drag it here.' : node.formElement.config.titles.drag;
         data.more_label   = (!node.formElement.config.titles.more)? 'Add more files' : node.formElement.config.titles.more;
         data.empty_label  = (!node.formElement.config.titles.more)? 'Remove all files' : node.formElement.config.titles.empty;
         data.remove_label = (!node.formElement.config.titles.remove)? 'Remove this file' : node.formElement.config.titles.remove;
         //data.uploading = (!node.formElement.uploadText)? 'Uploading&hellip;' : node.formElement.uploadText;
         //data.done   = (!node.formElement.doneText)? 'Done!' : node.formElement.doneText;
         //data.error  = (!node.formElement.errorText)? 'Error!' : node.formElement.errorText;
         data.upload_label = (node.formElement.config.titles.upload) ? node.formElement.config.titles.upload : 'Upload';
         data.upload = (node.formElement.config.upload) ? true : false;
         node.formElement.config.titles.tplural   = (!node.formElement.config.titles.tplural) ? 'Files': node.formElement.config.titles.tplural;
         node.formElement.config.titles.tsingular = (!node.formElement.config.titles.tsingular)?'File': node.formElement.config.titles.tsingular;
         node.formElement.config.preload          = (!node.formElement.config.preload)? false : JSON.parse(node.formElement.config.preload);
         node.data = data;
      },
      'onInsert': function(evt, node) {
         var _this    = $('#'+node.id+'.fileupload .box');
         var loaded   = 0;
         var allFiles = [];
         var badType  = 'Wrong file type for %filename%. Only png/jpeg/gif are admited';

         sessionStorage.removeItem('filesToUpload');
         
         /*---- used functions ----*/
         var isAdvancedUpload = function() {
            var div = document.createElement('div');
            return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
         }

         var checkFileInStorage = function(inputId, fileName) {
            var rawdata  = sessionStorage.getItem('filesToUpload');
            var storedArr= (rawdata) ? JSON.parse(rawdata):[];
            var fileEsta = false;

            ///// if file was selected avoid to load it
            for(var idx in storedArr) {
               if (storedArr[idx].element == inputId) {
                  var storedFiles = storedArr[idx].files;

                  for(var jx in storedFiles) {
                     var storedFile = storedFiles[jx];

                     if (storedFile == fileName) {
                        fileEsta = true;
                        break;
                     }
                  }

                  if (fileEsta)
                  break;
               }
            }

            return fileEsta;
         };

         var checkElemInStorage = function(inputId) {
            var rawdata  = sessionStorage.getItem('filesToUpload');
            var storedArr= (rawdata) ? JSON.parse(rawdata):[];
            var elemEsta = false;

            ///// is file was selected avoid to load it
            for(var idx in storedArr) {
               if (storedArr[idx].element == inputId) {
                  elemEsta = true;
                  break;
               }
            }

            return (elemEsta) ? idx : -1;
         };

         var restoreStorage = function() {
            var rawdata  = sessionStorage.getItem('filesToUpload');
            var storedArr= (rawdata) ? JSON.parse(rawdata):[];

            if (storedArr.length) {
               var idx = checkElemInStorage(node.id);

               if (idx != -1) {
                  if (!storedArr[idx].files.length)
                  storedArr.splice(idx,1);

                  if (!storedArr.length)
                  sessionStorage.removeItem('filesToUpload');
               }
            }

            $('#'+node.id+'.fileupload .box .box-thumb > span label').html('');
            $('#'+node.id+'.fileupload .box .box-thumb').hide();
            $('#'+node.id+'.fileupload .box .box-input').show();

            $('#control-'+ node.name).val('');
            $('#control-'+ node.name).focusout();
         };

         var delFileFromStorage = function(fileName) {
            var rawdata  = sessionStorage.getItem('filesToUpload');
            var storedArr= (rawdata) ? JSON.parse(rawdata):[];
            var filesArr = [];

            if (storedArr.length) {
               var idx = checkElemInStorage(node.id);
               
               if (idx != -1) {
                  var elemFiles = storedArr[idx].files;

                  for (var jdx in elemFiles) {
                     if (elemFiles[jdx] != fileName) {
                        filesArr.push(elemFiles[jdx]);
                     }
                  }

                  storedArr[idx].files = filesArr;
                  rawdata = JSON.stringify(storedArr);
                  sessionStorage.setItem('filesToUpload', rawdata);
                  
                  $('#'+node.id+'.fileupload .box .box-thumb ul li').each(function() {
                     if ($(this).find('img').data('filename') == fileName) {
                        $(this).remove();

                        if ( !$('#'+node.id+'.fileupload .box .box-thumb ul li').length ) {
                           restoreStorage();
                        }
                     }
                  });
               }
            }
         };

         var addFile2Storage = function(fileName) {
            var rawdata  = sessionStorage.getItem('filesToUpload');
            var storedArr= (rawdata) ? JSON.parse(rawdata):[];
            var filesArr = [];
            var elemArr  = [];

            if (storedArr.length) {
               var idx = checkElemInStorage(node.id);

               if (idx != -1) {
                  var elemFiles = storedArr[idx].files;
                  elemFiles.push(fileName);

                  storedArr[idx].files = elemFiles;
               }
            }
            else {
               $('#'+node.id+'.fileupload .box .box-thumb ul li').each(function(li) {
                  filesArr.push( $(this).find('div img').data('filename') );
               });

               var elemObj = {'element':node.id, 'files':filesArr};
               storedArr.push(elemObj);
            }

            rawdata = JSON.stringify(storedArr);
            sessionStorage.setItem('filesToUpload', rawdata);

            $('#control-'+ node.name).val( $('#'+node.id+'.fileupload .box .box-thumb ul li').length );
            $('#control-'+ node.name).focusout();
         };

         var gralCallback = function(fileData) {
            var thumbList = $('#'+node.id+'.fileupload .box .box-thumb ul li');

            loaded  = loaded + fileData.sizeKb;
            cantlbl = (thumbList.length > 1) ? node.formElement.config.titles.tplural : node.formElement.config.titles.tsingular;

            $('#'+node.id+'.fileupload .box .box-thumb > span label').html(thumbList.length+' '+ cantlbl +' - '+ loaded +' KB');
            $('#'+node.id+'.fileupload .box .box-input').hide();
            $('#'+node.id+'.fileupload .box .box-thumb').show();

            addFile2Storage(fileData.name);
         }

         var add2ThumbList = function( cv, fileData, gralCb ) {
            var list  = $('.box-thumb ul'),
                src   = cv ? cv.toDataURL( fileData.type ) : cv.toDataURL(),
                tname = fileData.name.split('.'),
                thumb = new Image();
   
            if (node.formElement.config.thumb && node.formElement.config.thumb.size) {
               var thumb_w = (node.formElement.config.thumb.size.width) ? node.formElement.config.thumb.size.width:0;
               var thumb_h = (node.formElement.config.thumb.size.height) ? node.formElement.config.thumb.size.height:0;
            }
            else {
               var thumb_w = 100; /// default width to 100px
               var thumb_h = 0; /// default height to 0px, to scale using the default width (100px)
            }

            if (thumb_w && !thumb_h) {
               var prop    = cv.width / thumb_w;
               var thumb_h = cv.height / prop;
            }
            else if (thumb_h && !thumb_w) {
               var prop    = cv.height / thumb_h;
               var thumb_w = cv.width / prop;
            }

            var name = tname[0] + '.' + tname[1];
            var size = Math.round( fileData.size / 1024 );
   
            fileData.sizeKb = size;
   
            thumb.src = src;
            thumb.height= thumb_h;
            thumb.width = thumb_w;
            thumb.title = name +' '+ size +' KB';
            thumb.alt = name +' '+ size +' KB';
            thumb.setAttribute('data-filename', name);
            thumb.setAttribute('data-size', fileData.size);
              
            var item = document.createElement('li');
            var cont = document.createElement('div');
            var data = document.createElement('label');
            var info = document.createElement('strong');
            var cntd = document.createElement('span');
            var dele = document.createElement('a');
   
            dele.setAttribute('class','remove-file');
            dele.setAttribute('href','#');
            dele.setAttribute('title',node.data.remove_label);
            dele.setAttribute('alt',node.data.remove_label);
            dele.setAttribute('rel',name);

            info.appendChild(document.createTextNode(size+' KB'));
            cntd.appendChild(dele);
            data.appendChild( document.createTextNode(name +' [ '));
            data.appendChild( info);
            data.appendChild( document.createTextNode(' ]'));
            cont.appendChild( thumb );
            item.appendChild( cont );
            item.appendChild( data );
            item.appendChild( cntd );
            list.append( $(item) );
   
            gralCb( fileData );
         };
         
         var image2Canvas = function( imgUrl, fileData, gralCb ) {
            var canvas = document.createElement('canvas');

            canvas.width = imgUrl.naturalWidth;
            canvas.height = imgUrl.naturalHeight;
                 
            canvas.getContext('2d').drawImage( imgUrl, 0, 0, imgUrl.naturalWidth, imgUrl.naturalHeight );
            add2ThumbList( canvas, fileData, gralCb );
         };

         var buildImageTag = function(blob, fileData, gralCb, url) {
            var img = new Image();

            img.src = blob;
            img.onload = function() {
               image2Canvas( this, fileData, gralCb );

               if (url)
               url.revokeObjectURL(this.src);
            };
         }

         var isImageFile = function(fileData) {
            var isImage = false;

            switch(fileData.type) {
               case 'image/png':
               case 'image/jpeg':
               case 'image/gif':
                  isImage = true;
                  break;
            }

            if ($('#'+node.id+'.fileupload .box .box-input').is(':visible'))
                 var selector = $('#'+node.id+'.fileupload .box .box-input');
            else var selector = $('#'+node.id+'.fileupload .box .box-thumb');

            if (!isImage) {
               badtype = (!node.formElement.config.titles.badtype) ? badType : node.formElement.config.titles.badtype;
               badtype = badtype.replace('%filename%', '<strong>'+fileData.name+'</strong>');

               selector.append('<label class="error">'+ badtype +'</label>');
               return false;
            }
            else {
               selector.find('.error').remove();
               return true;
            }
         };

         var handleFiles = function(fileList) {
            var url = window.URL || window.webkitURL;

            for(var idx=0; idx < fileList.length; idx++) {
               var fileData = fileList[ idx ];

               if (!isImageFile(fileData)) {
                  return false;
               }

               if (!checkFileInStorage(node.id, fileData.name)) {
                  if (url) {
                     buildImageTag( url.createObjectURL(fileData), fileData, gralCallback, url );
                  }
                  else {
                     var reader = new FileReader(); 
   
                     reader.readAsDataURL( fileData );
                     reader.onloadend = function ( ev ) {
                        buildImageTag( url.createObjectURL(fileData), fileData, gralCallback, url);
                     }
                  }
               }
            }
         }

         var handlePreloaded = function() {
            var preloaded = node.formElement.config.preload;
            if (preloaded && preloaded.length) {
               for (var idx in preloaded) {
                  var filedata = {
                     name:   preloaded[idx].name,
                     sizeKb: preloaded[idx].sizeKb,
                     size:   preloaded[idx].size,
                     type:   preloaded[idx].mime
                  };
                  
                  buildImageTag( preloaded[idx].url, filedata, gralCallback);
               }
            }
         }

         handlePreloaded();

         /*---- common processing for single remove action ----*/
         $('.fileupload').on('click','.remove-file',function(e) {
            e.preventDefault();

            delFileFromStorage($(this).attr('rel'));
         });

         /*---- common processing for total remove action ----*/
         $('#empty-list').click(function(e) {
            e.preventDefault();

            $('.fileupload .remove-file').each(function() {
               delFileFromStorage($(this).attr('rel'));
            });
         });

         /*---- common processing for anchor 'More elements' ----*/
         $('#add-more').click(function(e) {
            e.preventDefault();

            $('#'+node.id+'.fileupload .box .box-thumb .error').remove();
            $('#fileupload-'+node.name).trigger('click');
         });

         /*---- common processing for input file loading ----*/
         $('#fileupload-'+node.name).click(function() {
            $('#'+node.id+'.fileupload .box .box-input span.error').remove();
         });

         $('#fileupload-'+node.name).change(function(e) {
            var loadedFiles = e.originalEvent.target.files;
            handleFiles(loadedFiles);
         });

         /*---- common processing for drag & drop ----*/
         if (isAdvancedUpload()) {
            $('#'+node.id+'.fileupload .box .box-input span.error').remove();

            _this.addClass('has-advanced-upload');
            _this
            .on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
               e.preventDefault();
               e.stopPropagation(); 
            })
            .on('dragover dragenter', function() { _this.addClass('is-dragover'); })
            .on('dragleave dragend drop', function() { _this.removeClass('is-dragover'); })
            .on('drop', function(e) {
               //_this.append($('<div class="box-thumb"><ul></ul></div>'));
               var droppedFiles = e.originalEvent.dataTransfer.files;
               handleFiles(droppedFiles);
            });
         }
         else {
            var iframeName = 'uploadiframe' + new Date().getTime();
            $iframe = $('<iframe name="' + iframeName + '" style="display: none;"></iframe>');

            $('body').append($iframe);
            $form.attr('target', iframeName);

            $iframe.one('load', function() {
               var data = JSON.parse($iframe.contents().find('body' ).text());

               $(this).removeClass('is-uploading')
                      .addClass(data.success == true ? 'is-success' : 'is-error')
                      .removeAttr('target');

               if (!data.success) $errorMsg.text(data.error);

               $(this).removeAttr('target');
               $iframe.remove();
            });
         }
      }
   };

   JSONForm.fieldTypes['ranking'] = {
      'template': '<div id="ranking-container">'+
                     '<input id="<%=node.id%>" name="<%=node.name%>" class="ranking" type="hidden" value="" <%= (node.schemaElement && node.schemaElement.required ? " required=\'required\'" : "") %>/>'+
                     '<div>'+
                     '<span>'+
                     '<ul>'+
                     '<%_.each(node.schemaElement.data.items, function(item, key) {%>'+
                       '<li rel="<%= item.code %>"><%= item.text %></li>'+
                     '<%}); %>'+
                     '</ul>'+
                     '</span>'+
                     '<span>'+
                     '<ul>'+
                     '<%_.each(node.schemaElement.data.points, function(itemPoints, key) {%>'+
                        '<li class="<%=node.schemaElement.data.items[key].code%>">'+
                        '<ul>'+
                        '<%_.each(itemPoints, function(point, key2) {%>'+
                        '<li><input type="radio" class="point" name="point_<%=node.schemaElement.data.items[key].code%>" value="<%=node.schemaElement.data.items[key].code+\':\'+point.value%>"/><%=point.text%></li>'+
                        '<%}); %>'+
                        '<li class="icon"><i></i></li>'+
                        '</ul>'+
                        '</li>'+
                     '<%}); %>'+
                     '</ul>'+
                     '</span>'+
                     '</div>'+
                  '</div>',
      'onInsert': function(evt, node) {
         var points = [];
         var califs = [];

         $('#ranking-container .point').each(function() {
            var name = $(this).attr('name');

            if (points.indexOf(name) == -1) {
               points.push(name);
            }
         });

         $('#ranking-container .point').click(function() {
            if ($(this).is(':checked')) {
               var voteArr  = $(this).val().split(':');
               var currCalif = califs; //$('#'+node.id).val();

               var parentLi = $(this).parent().parent().parent();
               parentLi.attr('class',voteArr[0]+' p'+voteArr[1]);

               if (currCalif != '')
                    var califArr = currCalif.split('|');
               else var califArr = [];

               var isHere = false;

               for (var i=0; i<califArr.length; i++) {
                  var votedArr = califArr[i].split(':');

                  if (voteArr[0] == votedArr[0]) {
                     votedArr[1] = voteArr[1];
                     isHere = true;
                     break;
                  }
               }

               if (isHere)
                    califArr[i] = voteArr.join(':');
               else califArr.push(voteArr.join(':'));

               califs = califArr.join('|');

               if (califArr.length == points.length) {
                  $('#'+node.id).val(califs);
               }
            }
         });
      },
      'fieldtemplate': true,
      'inputfield': true
   };

   JSONForm.fieldTypes['calendar'] = {
      'template': '<input type="text" class="calendar" name="<%=node.name%>" id="<%=node.id%>" value="" ' +
                  '<%= (node.placeholder? "placeholder=" + \'"\' + escape(node.placeholder) + \'"\' : "")%>' +
                  '<%= (node.schemaElement && node.schemaElement.format ? " format=\'"+node.schemaElement.format+"\'" : "")%> ' +
                  '<%= (node.schemaElement && node.schemaElement.required ? " required=\'required\'" : "")%> ' +
                  'type="text">'+
                  '<span class="datepicker-button"></span>',
      'fieldtemplate': true,
      'inputfield': true,
      'onInsert': function(evt, node) {
         if (typeof $.datepicker._updateDatepicker_original == 'undefined') {
            $.datepicker._updateDatepicker_original = $.datepicker._updateDatepicker;

            $.datepicker._updateDatepicker = function(inst) {
               $.datepicker._updateDatepicker_original(inst);
   
               var titleElem = inst.dpDiv[0].childNodes[0].childNodes[2];
               var monthList = titleElem.childNodes[0];
               var yearList  = titleElem.childNodes[1];
               var monthName = monthList.options[monthList.selectedIndex].text; 
               var yearName  = yearList.options[yearList.selectedIndex].text; 
             
               $(titleElem).prepend('<span>'+yearName+'</span>');
               $(titleElem).prepend('<span>'+monthName+'</span>');
            }
         }

         $('#'+node.id).datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            nextText: "›",
            prevText: "‹",
            changeMonth: true,
            changeYear: true,
            beforeShow: function(el, dp) {
               var inputField = $(el);

               /// si se esta dentro de un popup, se vincula el datepicker con el modal
			      if (inputField.parents('.jsonform.modal').length > 0) {
                  inputField.parent().append($('#ui-datepicker-div'));
                  $('#ui-datepicker-div').addClass('datepicker-modal');
                  //$('#ui-datepicker-div').hide();
    	         }    
            }
         }).on('change', function() {
            $(this).valid();
         });

         $('#'+node.id).parent().find('span.datepicker-button').click(function(){
            $('#'+node.id).focus();
         });
      }
   };

   JSONForm.fieldTypes['radiobuttons2'] = {
      'template': '<div '+
         'class="form-group jsonform-error-<%= keydash %> radiobuttons2'+
         '<%= (fieldHtmlClass ? " "+fieldHtmlClass : "") %>'+
         '<%= (node.schemaElement && node.schemaElement.required && (node.schemaElement.type !== "boolean") ? " jsonform-required" : "") %>' +
         '">' +
         '<div class="controls">'+
         '<input type="hidden" id="<%= node.id %>" name="<%= node.name %>" name="<%= node.name %>"<%if (value) { %> value="<%=value%>" <% } %>>'+
         '<div>'+
         '<% _.each(node.options, function(key, val) { %>' +
         '<label class="radio btn'+
         '<% if (((key instanceof Object) && (value === key.value)) || (value === key)) {%> active<% } %>'+
         '<%= (node.schemaElement && node.schemaElement.required ? " required" : "") %>'+
         '" rel="<%= (key instanceof Object ? key.value : key) %>">' +
         /*
         '<input type="radio" style="position:absolute;left:-9999px;" ' +
         '<% if (((key instanceof Object) && (value === key.value)) || (value === key)) { %> checked="checked" <% } %> name="<%= node.name %>" value="<%= (key instanceof Object ? key.value : key) %>"'+
         '<%= (node.disabled? " disabled" : "")%>' +
         '/>' +*/
         '<span><%= (key instanceof Object ? key.title : key) %></span></label> ' +
         '<% }); %>' +
         '</div>'+
         '</div>'+
         '<% if (node.schemaElement && node.schemaElement.required) {%> <div class="required-mark after"></div> <% } %>'+
         '</div>',
      'fieldtempate': true,
      'inputfield': true,
      'onInsert': function (evt, node) {
         var activeClass = 'active';
         var elt = node.formElement || {};

         if (elt.activeClass) {
            activeClass += ' ' + elt.activeClass;
         }

         $(node.el.parentNode).find('label.radio').on('click', function () {
            $(this).parent().find('label').removeClass(activeClass);
            $(this).parent().find('label input').attr('checked',false);

            $(this).addClass(activeClass);
            $(this).parent().parent().find('input').attr('value',$(this).attr('rel')).change();
         });

         $('#'+node.ownerTree.domRoot.id).find('button[type=reset]').click(function() {
            $(this).parent().find('label input').attr('checked',false);
            $(this).parent().find('label').removeClass(activeClass);
            $(this).parent().parent().find('input').removeAttr('value');
         });
      }
   };

   JSONForm.fieldTypes['spriteselect'] = {
    'template': 
      '<div class="spriteselect">' +
      '<% if (spriteFile) { %>'+
      '<input type="hidden" name="<%= node.name %>" id="<%= node.id %>" value="<%= value %>"<%= (node.schemaElement && node.schemaElement.required ? " required=\'required\'" : "") %> class="spriteselect"/>' +
      '<% if (useBackground) { %>' +
      '<input type="hidden" name="<%= node.name %>bg" id="<%= node.id %>-bg" value="<%= valuebg %>" class="spriteselect-bg"/>' +
      '<% } %>' +
      '<div class="gral-container">' +
        '<div class="container">' +
        '<%if (selectContainer){%><%=selectContainer%><%}else{%><div class="choosen"<%= (valuebg ? \' style="background:\'+valuebg+\'"\':\'\')%>></div><%}%>'+
        '<span<%= (buttonClass ? \' class="\'+buttonClass+\'" \':\'\')%>rel=""><%if (selectTitle) {%><%=selectTitle%><%}%></span>'+
        '<% if (useBackground) { %>' +
        '<div class="palette">' +
          '<div class="page">' +
          '<% xx=0; _.each(node.formElement.palette, function(key, idx) { %>'+
            '<% if ((xx > 0) && (xx == color_columns)) { %>'+
            '<% xx=0;%>'+
            '</div><div class="page">'+
            '<% } %>'+
              '<span class="palette-color <%=xx%>" alt="<%=key%>" title="<%=key%>" style="background:<%=key%>"></span>'+
             '<% xx++;%>'+
          '<% }); %>' +
          '</div>' +
          '<div class="page">'+
          '<span class="fa fa-undo-alt palette-color reset" style="background:<%=resetColor%>" alt="<%=resetTitle%>" title="<%=resetTitle%>"></span>'+
          '</div>'+
        '</div>' +
        '<% } %>' +
        '</div>' +
        '<div class="itemlist">' +
          '<a class="close" href="#"></a>'+
          '<div class="page">' +
            '<a>'+
              '<% if (resetImage instanceof Object) { %>'+
              '<span<%if (itemClass) { %> class="<%=itemClass%>" <% } %>style="background:url(<%=spriteFile%>) no-repeat;'+
                           'background-position:<%=resetImage.x%>px <%=resetImage.y%>px;'+
                           'width:<%=resetImage.w%>px;'+
                           'height:<%=resetImage.h%>px" alt="<%=resetImage.name%>" title="<%=resetImage.name%>">'+
              '</span>'+
              '<% } else { if (resetImage.match(/[a-z0-9\-]/) ) { %>'+
              '<span class="<%if (itemClass) {%><%=itemClass%> <%}%><%= resetImage %> reset" alt="<%=resetTitle%>" title="<%=resetTitle%>"></span>'+
              '<% }} %>'+
            '</a>'+

          '<%xx=1; _.each(node.options, function(key, idx) { %>'+
            '<% if ((xx > 0) && (xx == columns)) { %>'+
            '<% xx=0;%>'+
            '</div><div class="page">'+
            '<% } %>'+
            '<a>'+
              '<% if (key instanceof Object) { %>'+
              '<span<%if (itemClass) { %> class="<%=itemClass%>" <% } %>style="background:url(<%=spriteFile%>) no-repeat;'+
                           'background-position:<%=key.x%>px <%=key.y%>px;'+
                           'width:<%=key.w%>px;'+
                           'height:<%=key.h%>px" alt="<%=key.name%>" title="<%=key.name%>">'+
              '</span>'+
              '<% } else { if (key.match(/[a-z0-9\-]/) ) { %>'+
              '<span class="<%if (itemClass) {%><%=itemClass%> <%}%><%= key %>" alt="<%=key%>" title="<%=key%>"></span>'+
              '<% }} %>'+
              '<% xx++;%>'+
            '</a>'+
          '<% }); %>' +
          '</div>' +
        //'<% if (buttonClass) {%>' +
        '</div>' +
        //'<%}%>'+
      '</div>'+
      '<% } %>'+
      '</div>',
    'fieldtemplate': true,
    'inputfield': true,
    'onBeforeRender': function (data, node) {
      if (node.value && node.value.indexOf(':') != -1) {
         var value = data.value.split(':');
         data.valuebg = value[1];
         data.value = value[0]+' in-marquee';
      }
      else {
         var value = false;
         data.valuebg = '';
         data.value = '';
      }

      var needSpriteFile = false;

      var elt = node.formElement || {};
      var esq = node.schemaElement || {};
      var nbRows = null;
      var maxImgCols = elt.imageSelectorColumns || 5;
      var maxBgCols = elt.colorSelectorColumns || 5;
      data.buttonTitle = elt.imageSelectorTitle || 'Select...';
      data.spriteFile = elt.sprite || false;
      data.itemClass = elt.itemClass || false;
      data.buttonClass = data.value || elt.buttonClass || false;
      data.defWidth  = elt.width || 32;
      data.defHeight = elt.height || 32;
      data.selectTitle = elt.selectTitle || false;
      data.selectContainer = elt.container || false;
      data.useBackground = elt.palette.length || false;
      data.resetImage = elt.resetImage || false;
      data.resetColor = elt.resetColor || '#C4C4C4';
      data.resetTitle = elt.resetTitle || 'Empty';

      if (data.selectContainer && data.valuebg) {
         data.selectContainer = data.selectContainer.replace("\"choosen\"","\"choosen\" style=\"background:"+data.valuebg+"\"");
      }
      
      var definido = function(value) {
         return !(_.isUndefined(value) || _.isNull(value));
      }

      for (var i=0; i<node.options.length; i++) {
         if (node.options[i] instanceof Object) {
            needSpriteFile = true;

            if (!definido(node.options[i].w))
            node.options[i].w = data.defWidth;

            if (!definido(node.options[i].h))
            node.options[i].h = data.defHeight;

            if (!definido(node.options[i].x))
            node.options[i].x = i+node.options[i].w * -1;

            if (!definido(node.options[i].y))
            node.options[i].y = i+node.options[i].h * -1;
         }
      }

      if (!needSpriteFile) {
         data.spriteFile = '-'; // fill with something to bypass sprite file control
      }

      if (node.options.length > maxImgCols) {
         nbRows = Math.ceil(node.options.length / maxImgCols);
         data.columns = Math.ceil(node.options.length / nbRows);
      }
      else {
         data.columns = maxImgCols;
      }

      if (elt.palette.length > maxBgCols) {
         nbRows = Math.ceil(elt.palette.length / maxBgCols);
         data.color_columns = Math.ceil(elt.palette.length / nbRows);
      }
      else {
         data.color_columns = maxBgCols;
      }
    },
    'getElement': function (el) {
      return $(el).parent().get(0);
    },
    'onInsert': function (evt, node) {
      if ($('#'+node.id).val() || $('#'+node.id+'-bg').val()) {
         if (node.formElement.target)
              var target = $(node.formElement.target);
         else var target = $('.choosen');

      }

      if (node.formElement.buttonInside) {
         $(node.el).find('.container .choosen').append($(node.el).find('.container>span')) ;
      }

      $('.choosencont a.open').click(function(e) {
         e.preventDefault();

         if ($('.spriteselect .itemlist').not(':visible'))
         $('.spriteselect .itemlist').css('display', 'inline-block');

         return false;
      });

      $('.itemlist a.close').click(function(e) {
         e.preventDefault();

         if ($('.spriteselect .itemlist').is(':visible'))
         $('.spriteselect .itemlist').hide();

         return false;
      });

      $(node.el).on('click', '.spriteselect span.reset', function(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        
        var father = $(this).parent();
        var isColor = father.hasClass('page');
        var input = $('#'+node.id);
        
        if (node.formElement.target)
             var target = $(node.formElement.target);
        else var target = $('.choosen');

        if ( isColor ) {
           target = target.parent();
           input = $('#'+node.id+'-bg');
        }
            
        if (typeof evt.target.attributes.style !== 'undefined') {
           target.removeAttr('style');
        }
        else if (evt.target.className != '') {
           if (node.formElement.buttonClass)
                target.attr('class', node.formElement.buttonClass);
           else target.attr('class', evt.target.className.replace(' reset',''));
        }

        input.attr('value', '');

        $('#'+node.id).valid();
      });

      $(node.el).on('click', '.spriteselect span', function(evt) {
        evt.preventDefault();
        evt.stopPropagation();

        if ($(this).hasClass('reset')) {
           return false;
        }

        var father  = $(this).parent();
        var isColor = father.hasClass('page');
        var isAvatar= father.prop('tagName') == 'A';
        var input = $('#'+node.id);

        if (node.formElement.target)
             var target = $(node.formElement.target);
        else var target = $('.choosen');

        if ( isColor ) {
           target = target.parent();
           input = $('#'+node.id+'-bg');
        }
            
        if (typeof evt.target.attributes.style !== 'undefined') {
           target.attr('style', evt.target.attributes.style.value);
           var value = evt.target.attributes.title.value;
        }
        else if (evt.target.className != '') {
           if ( isColor )
                target.attr('class', evt.target.className);
           else target.attr('class', evt.target.className+' in-marquee');
           var value = evt.target.className;
        }

        if (isAvatar) {
           $('.spriteselect .itemlist').hide();
        }

        input.attr('value', value);

        $('#'+node.id).valid();
      });
     }
   },

   JSONForm.fieldTypes['captcha'] = {
      'template': '<div id="<%=node.id%>-container" class="rtk_captcha">'+
                    '<div class="eq-so-container">'+
                      '<div class="eq">'+
                         '<div>'+
                         '<span class="<%= eq.first_digit%>"></span>'+
                         '<span class="<%= eq.operator%>"></span>'+
                         '<span class="<%= eq.second_digit%>"></span>'+
                         '</div>'+
                         '<span class="equal"></span>'+
                         '<span class="result"><label>?</label></span>'+
                      '</div>'+
                      '<div class="so">'+
                         '<ul>'+
                         '<%_.each(ans, function(val, key) {%>'+
                            '<li><%= val %></li>'+
                         '<%}); %>'+
                         '</ul>'+
                      '</div>'+
                    '</div>'+
                    '<div class="re" alt="Reload" title="Reload"></div>'+
                    '<input type="hidden" class="captcha" name="<%=node.name%>" id="<%=node.id%>" value="" <%= (node.schemaElement && node.schemaElement.required ? " required=\'required\'" : "") %>/>'+
                  '</div>',
      'fieldtemplate': true,
      'inputfield': true,
      'onBeforeRender': function (data, node) {
         $.ajax({
            type:'GET',
            url:'/services/get_captcha.html',
            dataType:'json',
            async: false,
            success: function( response ) {
              data.eq = response.operation;
              data.ans= response.results;
              right = response.right;
            }
         });
      },
      'onInsert': function(evt, node) {
         var list_size= $('#'+node.id+'-container .so ul li').length;

         /// method to reload captcha
         $('#'+node.id+'-container .re').click(function() {
            $.ajax({
               type:'GET',
               url:'/services/get_captcha.html',
               dataType:'json',
               async: false,
               success: function( response ) {
                  $('#'+node.id+'-container .so ul li').each(function(key) {
                     $(this).html(response.results[key]);
                     $(this).removeClass('selected');
                  });

                  $('#'+node.id+'-container .eq span:eq(0)').removeAttr('class').addClass(response.operation.first_digit);
                  $('#'+node.id+'-container .eq span:eq(1)').removeAttr('class').addClass(response.operation.operator);
                  $('#'+node.id+'-container .eq span:eq(2)').removeAttr('class').addClass(response.operation.second_digit);
                  $('#'+node.id+'-container .eq .result label').html('?');
                  $('#'+node.id).val('');
                  $('#'+node.id).valid();
               }
            });
         });

         /// method to resolve captcha
         $('#'+node.id+'-container .so ul li').click(function() {
            var curr_sol = $(this).html();

            $(this).parent().find('li').each(function(key) {
               if ($(this).html() != curr_sol) {
                  $(this).removeClass('selected');
                  $('#'+node.id).val('');
                  $('#'+node.id+'-container .eq .result label').html('?');
               }
            });

            if ($(this).hasClass('selected')) {
               $(this).removeClass('selected');
               $('#'+node.id).val('');
               $('#'+node.id+'-container .eq .result label').html('?');
            }
            else {
               $(this).addClass('selected');

               var eq = $('#'+node.id+'-container .eq div span:eq(0)').attr('class')+':'+
                        $('#'+node.id+'-container .eq div span:eq(1)').attr('class')+':'+
                        $('#'+node.id+'-container .eq div span:eq(2)').attr('class')+':'+
                        $(this).html();

               $('#'+node.id).val(eq);
               $('#'+node.id+'-container .eq .result label').html($(this).html());
            }

            if (typeof $('#'+node.id).valid === 'function')
            $('#'+node.id).valid();

            return false;
         });
      }
   };

   JSONForm.fieldTypes['checkboxgroup'] = {
    'template': '<div class="checkboxgroup"><%= choiceshtml %></div>',
    'fieldtemplate': true,
    'inputfield': true,
    'onBeforeRender': function (data, node) {
      // Build up choices from the enumeration list
      var choices = null;
      var choiceshtml = null;
      var template = '<div class="checkbox"><label>' +
        '<input type="checkbox" <% if (checked) { %> checked="checked" <% } %> name="<%= name %>" value="<%= value %>"' +
        '<%= (node.disabled? " disabled" : "")%>' +
        '/><%= title %></label></div>';
      if (!node || !node.schemaElement) return;

      if (node.schemaElement.items) {
        choices =
          node.schemaElement.items["enum"] ||
          node.schemaElement.items[0]["enum"];
      } else {
        choices = node.schemaElement["enum"];
      }
      if (!choices) return;

      var fieldTemplateSettings = {
          evaluate    : /<%([\s\S]+?)%>/g,
          interpolate : /<%=([\s\S]+?)%>/g
      };

      choiceshtml = '';

      _.each(choices, function (choice, idx) {

        choiceshtml += _.template(template, fieldTemplateSettings)({
          name: node.key + '[]',
          checked: _.includes(node.value, choice),
          value: choice,
          title: node.formElement.titleMap.hasOwnProperty(choice) ? node.formElement.titleMap[choice] : choice,
          node: node
        });
      });

      data.choiceshtml = choiceshtml;
    }
  };

   JSONForm.fieldTypes['swcheckbox'] = {
      'template': '<%= (grouped ? "<div class=\'control-group\'><div class=\'controls\'>":"") %>'+
                  '<div id="<%=node.id%>" class="swcheckbox checkbox-switchery checkbox-<%=position%> switchery-<%=size%>">' +
                  '<label class="display-block">' +
                  '<input type="checkbox" class="switchery" name="<%=node.name%>" id="<%=node.name%>" <%=(checked ? "checked=\'checked\'":"")%> value="<%= value %>"<%=data%>>' +
                  '<%= inlinetitle %>' +
                  '</label>' +
                  '</div>'+
                  '<%= (grouped ? "</div></div>":"") %>',
      'onInsert': function (evt, node) {
         $('#'+node.ownerTree.domRoot.id).find('button[type=reset]').click(function() {
            $('#'+node.ownerTree.domRoot.id).find('input.switchery').attr('checked', false).trigger("click").trigger('click');
         });

         var elem = $('#'+node.id).find('.switchery');
         var data = elem.data();
         var opts = {};

         if (data.onColor)  opts.color          = elem.data('onColor');
         if (data.offColor) opts.secondaryColor = elem.data('offColor');
         if (data.dimention)opts.size           = elem.data('dimention');
   
         var switchery = new Switchery(elem[0], opts);
      },      
      'onBeforeRender': function(data, node) {
         var elt = node.formElement || {};
         var dat = [];
         var chk = '';

         if (node.schemaElement.default || elt.checked)
              chk = "checked=\"checked\"";
         else chk = '';

         data.position = elt.position || 'right';
         data.size = elt.size || 'xs';
         data.checked = chk;
         data.inlinetitle = elt.inlinetitle || '';
         data.grouped = elt.grouped || false;

         if (elt.dimention)
         dat.push('data-dimention="'+elt.dimention+'"');

         if (elt.on_color)
         dat.push('data-on-color="'+elt.on_color+'"');

         if (elt.off_color)
         dat.push('data-off-color="'+elt.off_color+'"');

         data.data = dat.join(' ');
      }
   };

   JSONForm.fieldTypes['tabcontent'] = {
      'template': '<div id="<%=panecode%>" class="tab-pane<%=(node.formElement.active)?" active":""%>" data-idx="<%=node.childPos%>">' +
                  '<%=children%>'+
                  '</div>',
      'onBeforeRender': function (data, node) {
          var father = node.parentNode.formElement;
          data.panecode = ((father.prefix) ? father.prefix+'-':'') + node.formElement.id;
      }
   };

   JSONForm.fieldTypes['fieldtab'] = {
      'template': '<div id="<%=node.id%>" class="tabbable">'+
                  '<ul class="<%=node.id%>-tabs <%=node.formElement.htmlClass%>">'+
                     '<%_.each(tabs, function(tab, key) {%>'+
                       '<li class="<%=(tab.active)?"active":""%>">'+
                       '<a href="#<%=tab.code%>" data-toggle="tab"><%= tab.title %></a>'+
                       '</li>'+
                     '<%}); %>'+
                     '</ul>'+
                     '<div class="tab-content">'+
                       '<%=children%>'+
                     '</div>'+
                  '</div>',
    'getElement': function (el) {
      return $(el).parent().get(0);
    }, 
    'onBeforeRender': function (data, node) {
         var elt = node.formElement || {};
         var tabs= [];
         var children = null;
         var currtab  = null;
         var contents = {};

         data.customClass = '';

         _.each(node.children, function (item, idx) {
            var tab = {};
            tab.code = ((elt.prefix) ? elt.prefix+'-':'') + item.formElement.id;
            tab.active = (item.formElement.active) ? item.formElement.active : false;
            tab.title = (item.formElement.title) ? item.formElement.title : '';
            tabs.push(tab);
         });

         data.tabs = tabs;
      },
      'onInsert': function(evt, node) {
         $('#'+node.id+' ul.'+node.id+'-tabs li a').click(function (e) {
            e.stopImmediatePropagation();
            e.preventDefault();
            $(this).tab('show');
         })
      }
   }

   JSONForm.fieldTypes['fileupload'] = {
      'template': '<a id="<%=node.id%>-browse" class="filebrowse <%= buttonClass %>" href="javascript:;">'+
                  '<%= browse_text %>'+
                  '</a>'+
                  '<input type="hidden" value="<%= value %>" id="<%= id %>-default" name="<%= node.name %>_default" data-value="<%= value%>">'+
                  '<input type="file" class="fileupload<%= customClass %>" id="<%= id %>" name="<%= node.name %>"'+
                  '<%= (node.schemaElement && node.schemaElement.required ? " required=\'required\'" : "") %>' +
                  '<%= (node.formElement && node.formElement.multiple ? " multiple=\'multiple\'" : "") %>' +
                  '<%= (fieldHtmlClass ? " class=\'" + fieldHtmlClass + "\'" : "") %>' +
                  '/>'+
                  '<span class="fileinfo<%= (hasCurrFile && !prev_float ? \' new\':\'\')%>" id="<%=node.id%>-info" data-text="<%=choose_text%>">' +
                  '<span class="showname"><%= choose_text %></span>' +
                  '</span>'+
                  '<% if (hasCurrFile) { %>'+
                  '<span class="fileinfo current<%= (prev_float ? \' float\':\'\')%>" id="<%=node.id%>-current">'+
                  '<strong data-toggle="tooltip" title="<%=file_name%>" alt="<%=file_name%>"><i class="fas fa-image"></i>&nbsp;<%=file_name%></strong>'+
                  '<ul>'+
                  '<li class="preview"'+
                  '<%=(rawFile ? " data-rawfile=\'" + rawFile + "\'" : "")%>'+
                  '<%=(urlFile ? " data-urlfile=\'" + urlFile + "\'" : "")%>'+
                  '><i class="fa fa-eye"></i></li>'+
                  '<li class="delete"><i class="fa fa-times"></i></li>'+
                  '</ul>'+
                  '</span>'+
                  '<% } %>'+
                  '<%= (hasDropZone ? "<div class=\'drop-zone\'>drop_text</div>" : "") %>',
      'onBeforeRender': function (data, node) {
         var elt = node.formElement || {};

         data.customClass = elt.fieldHtmlClass ? ' '+elt.fieldHtmlClass : '';
         data.buttonClass = elt.buttonClass    ? ' '+elt.buttonClass : 'btn btn-primary';
         data.hasCurrFile = node.value         ? node.value:null;
         data.hasDropZone = elt.dropzone;

         data.file_name   = node.value       ? node.value:null;
         data.browse_text = elt.browseText   ? elt.browseText   : 'Browse...';
         data.choose_text = elt.placeholder  ? elt.placeholder  : 'Choose a file to upload';
         data.drop_text   = elt.dropText     ? elt.dropText     : 'Drag & drop a file here';
         data.prev_float  = elt.previewFloat ? elt.previewFloat : false;
         data.rawFile     = elt.rawFile      ? elt.rawFile      : null;
         data.urlFile     = elt.urlFile      ? elt.urlFile      : null;
         data.typeFile    = elt.typeFile     ? elt.typeFile     : null;
      },
      'onInsert': function(evt, node) {
         var crrObj = $('#'+node.id+'-current');
         var btnObj = $('#'+node.id+'-browse');
         var infObj = $('#'+node.id+'-info');
         var inpObj = $('#'+node.id);
         var hidObj = $('#'+node.id+'-default');
         var frmObj = $('#'+node.id).closest('form');
         var prnObj = $('#'+node.id).parent();

         var validMimeType = function (fileInfo, ableMimes) {
            if (ableMimes == null) return true;

            var currMimeTypes = {
            'audio/aac'                      : ['aac'],
            'application/x-abiword'          : ['abw'],
            'application/x-freearc'          : ['arc'],
            'image/avif'                     : ['avif'],
            'video/x-msvideo'                : ['avi'],   
            'application/vnd.amazon.ebook'   : ['azw'],   
            'application/octet-stream'       : ['bin'],   
            'image/bmp'                      : ['bmp'],   
            'application/x-bzip'             : ['bz'],    
            'application/x-bzip2'            : ['bz2'],  
            'application/x-cdf'              : ['cda'],
            'application/x-csh'              : ['csh'],    
            'text/css'                       : ['css'],    
            'text/csv'                       : ['csv'],    
            'application/msword'             : ['doc'],    
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document': ['docx'],   
            'application/vnd.ms-fontobject'  : ['eot'],    
            'application/epub+zip'           : ['epub'],   
            'application/gzip'               : ['gz'],     
            'image/gif'                      : ['gif'],    
            'text/html'                      : ['htm','html'],    
            'image/vnd.microsoft.icon'       : ['ico'],    
            'text/calendar'                  : ['ics'],    
            'application/java-archive'       : ['jar'],    
            'image/jpeg'                     : ['jpeg','jpg'],   
            'text/javascript'                : ['js','mjs'],    
            'application/json'               : ['json'],   
            'application/ld+json'            : ['jsonld'], 
            'audio/midi'                     : ['mid','midi'],
            'audio/x-midi'                   : ['mid','midi'],
            'audio/mpeg'                     : ['mp3'],    
            'video/mp4'                      : ['mp4'],    
            'video/mpeg'                     : ['mpeg'],   
            'application/vnd.apple.installer+xml'            : ['mpkg'],  
            'application/vnd.oasis.opendocument.presentation': ['odp'],    
            'application/vnd.oasis.opendocument.spreadsheet' : ['ods'],    
            'application/vnd.oasis.opendocument.text'        : ['odt'],     
            'audio/ogg'                      : ['oga'],    
            'video/ogg'                      : ['ogv'],    
            'application/ogg'                : ['ogx'],    
            'audio/opus'                     : ['opus'],   
            'font/otf'                       : ['otf'],    
            'image/png'                      : ['png'],    
            'application/pdf'                : ['pdf'],    
            'application/x-httpd-php'        : ['php'],    
            'application/vnd.ms-powerpoint'  : ['pptx'],   
            'application/vnd.rar'            : ['rar'],    
            'application/rtf'                : ['rtf'],   
            'application/x-sh'               : ['sh'],     
            'image/svg+xml'                  : ['svg'],    
            'application/x-tar'              : ['tar'],    
            'image/tiff'                     : ['tif','tiff'],    
            'video/mp2t'                     : ['ts'],
            'font/ttf'                       : ['ttf'],
            'text/plain'                     : ['txt'],
            'application/vnd.visio'          : ['vsd'],
            'audio/wav'                      : ['wav'],
            'audio/webm'                     : ['weba'],
            'video/webm'                     : ['webm'],
            'image/webp'                     : ['webp'],
            'font/woff'                      : ['woff'],
            'font/woff2'                     : ['woff2'],
            'application/xhtml+xml'          : ['xhtml'],
            'application/vnd.ms-excel'       : ['xls'],
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': ['xlsx'],
            'application/xml'                : ['xml'],
            'text/xml'                       : ['xml'],
            'application/vnd.mozilla.xul+xml': ['xul'],
            'application/zip'                : ['zip'],
            'video/3gpp'                     : ['3gp'],
            'video/3gpp2'                    : ['3g2'],
            'application/x-7z-compressed'    : ['7z']
            };
            var exists = false;

            if (!Array.isArray(ableMimes)) {
               throw new Error('Fileupload error: able mimetypes must be an array');
            }
            else {
               if (currMimeTypes[fileInfo.type] !== undefined) {
                  var currMimes = currMimeTypes[fileInfo.type];

                  for (var i in ableMimes) {
                     for(var j in currMimes) {
                        if (ableMimes[i] == currMimes[j]) {
                           exists = true;
                           break;
                        }
                     }

                     if (!exists) {
                        break;
                     }
                  }
               }
            }

            return exists;
         }

         if (crrObj.hasClass('float')) {
            crrObj.detach().appendTo('#'+ frmObj.attr('id'));
         }

         crrObj.find('strong').tooltip('enable');
         crrObj.find('strong').on('shown.bs.tooltip', function() {
               $(this).next().find('.tooltip-inner').css('display','inline-table');
         });

         infObj.on('click', '.clear-file', function() {
            console.log(infObj.data('choose_text'));

            inpObj.val(null);
            infObj.removeClass('choosen');
            infObj.find('.showname').text(infObj.data('text'));
            infObj.find('.showname').removeData('toggle');
            infObj.find('.showname').removeAttr('data-original-title');
            //infObj.find('.showname').removeAttr('title');
            infObj.find('.showname').tooltip('disable');
            $(this).remove();
         });

         prnObj.on('click', '.delete', function(){
            hidObj.val('');
            $('#'+node.id+'-current').remove();
            infObj.removeClass('new');

            if (typeof $('#'+node.id).valid === 'function') {
            $('#'+node.id).valid();
            }
         });

         btnObj.click(function (e) {
            e.stopImmediatePropagation();
            e.preventDefault();

            inpObj.click();
         });

         if (node.formElement.contentFloat) {
            $('#'+node.id+'-current').appendTo(node.formElement.contentFloat);
         }

         frmObj.bind('reset', function() {
            if (!$('#'+node.id+'-current').length) {
               var prev_float = node.formElement.previewFloat ? ' float':'';
               var file_name  = hidObj.data('value');
               var raw_file   = node.formElement.rawFile;
               var url_file   = node.formElement.urlFile;
               var type_file  = node.formElement.typeFile;
   
               $('#'+node.id+'-default').val(hidObj.data('value'));

               if (node.value) {
                  var currObj = $('<span class="fileinfo current'+prev_float+'" id="'+node.id+'-current">'+
                                  '<strong data-toggle="tooltip" title="'+file_name+'" alt="'+file_name+'"><i class="fas fa-image"></i>&nbsp;'+file_name+'</strong>'+
                                  '<ul>'+
                                  '<li class="preview"'+
                                  (raw_file ? ' data-rawfile="' + raw_file + '"' : '')+
                                  (url_file ? ' data-urlfile="' + url_file + '"' : '')+
                                  '><i class="fa fa-eye"></i></li>'+
                                  '<li class="delete"><i class="fa fa-times"></i></li>'+
                                  '</ul>'+
                                  '</span>');

                  if (node.formElement.contentFloat)
                       currObj.appendTo(node.formElement.contentFloat);
                  else currObj.insertAfter(infObj);

                  currObj.find('strong').tooltip('enable');
   
                  if (!node.formElement.previewFloat) infObj.addClass('new');
               }
            }

            inpObj.val(null);
            infObj.removeClass('choosen');
            infObj.find('.showname').text(infObj.data('text'));
            infObj.find('.clear-file').remove();
         });

         inpObj.change(function(e) {
            e.stopImmediatePropagation();
            e.preventDefault();

            var avoidTypes = node.formElement.mimetypes ? node.formElement.mimetypes : null;

            if (!validMimeType(e.target.files[0], avoidTypes))
                 inpObj.data('valid-format', false);
            else inpObj.data('valid-format', true);

            if (typeof $('#'+node.id).valid === 'function')
            $('#'+node.id).valid();

            var fileName = e.target.files[0].name;

            //infObj.data('choose_text', infObj.text());
            if (e.target.files.length) {
               infObj.addClass('choosen');
               infObj.find('.showname').text(e.target.files[0].name);
               infObj.find('.showname').data('toggle','tooltip');
               //infObj.find('.showname').attr('title',e.target.files[0].name);
               infObj.find('.showname').attr('data-original-title',e.target.files[0].name);

               if (!infObj.find('.clear-file').length)
               infObj.append('<i class="fa fa-times clear-file"></i>');

               infObj.find('.showname').tooltip('enable');
            }
         });
      },
      'fieldtemplate': true,
      'inputfield': true
   }
})(jQuery);

