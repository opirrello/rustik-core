/*********************************************
  changeSwitchery plugin
  Rustic framework plugin v1.0

  main initialization: line 363

*********************************************/
(function ($) {
    $.changeSwitchery = $.fn.changeSwitchery = function (checked) {
      var element = $(this);
       
      if ( ( element.is(':checked') && checked == false ) || ( !element.is(':checked') && checked == true ) ) {
         element.parent().find('span.switchery').trigger('click');
      }
    }
})(jQuery);


