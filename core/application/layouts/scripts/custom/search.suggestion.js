/*********************************************
  searchSuggestion plugin
  Rustic framework plugin v1.0

  main initialization: line 363

*********************************************/
(function ($) {
    $.searchSuggestion = $.fn.searchSuggestion = function() {
      var callback = false;
      var currElem = $(this);
      var options = {
         container: $(this).parent(),
         dropdown: $(this),
         noresults: false,
         placeholder: false,
         markMatch: false,
         url: false,
         method: 'GET',
         response: false,
         data: false,
         recipient: {}
      };

      //---------- PREVIOUS CONFIGURATION BEFORE MAIN PROCESS (initialize) ----------//
      /// extending options
      if (arguments.length < 1) return false;
      else if (arguments.length >= 1) {
         if (arguments.length == 1) {
            if (typeof arguments[0] === 'object') $.extend(options, arguments[0]);
            else if (typeof arguments[0] === 'function') callback = arguments[0];
         }
         else {
            if (typeof arguments[0] === 'object') $.extend(options, arguments[0]);
            if (typeof arguments[1] === 'function') callback = arguments[1];
         }
      }

      if (!options.response || typeof options.response != 'object') {
         throw 'no hay objeto de respuesta definida';
         return false;
      }
      else if (!options.response.name || !options.response.key || !options.response.text) {
         throw 'las propiedades de respuesta no estan definidas';
         return false;
      }

      markMatch = function(text, term) {
	      var match   = text.toUpperCase().indexOf(term.toUpperCase());
	      var $result = $('<span></span>');

	      if (match < 0) { return $result.text(text); }

	      var elementToReplace = text.substr(match, term.length);

	      text = text.replace(elementToReplace, '<strong>'+elementToReplace+'</strong>');

         $result.append(text);
	      return $result;
	   }

      //------------- Creating configuration file (options) ------------------
      var configuration = {
         language: 'es',
         width:'100%',
         dropdownParent: options.dropdown,
         placeholder: options.placeholder,
         minimumInputLength: 3,
         templateResult: function(item) {
            if (item.loading) { return item.text; }

	         var term = query.search || '';
	         var $result = markMatch(item.text, term);
            
	         return $result;
         },
         ajax: {
            url: options.url,
            method: options.method,
            dataType: 'json',
            data: function (params) {
               datax = new Object();

               if (options.data) {
                  $.each(options.data, function(i, e) {
                     if (typeof e === 'object') {
                        datax[i] = e.val();
                     }
                     else {
                        datax[i] = e;
                     }
                  });
               }

               query = {search:params.term, page:params.page};

               if (options.data) {
                  $.extend(query, datax);
               }

               return query;
            },
            processResults: function (response, params) {
               var result = [];

               if (!response || typeof response != 'object') {
                  throw 'el servicio no ha devuelto respuesta';
                  return false;
               }
               
               window[options.response.name] = response[options.response.name];

               for(var id in window[options.response.name]) {
                  result.push({
                     id:   window[options.response.name][id][options.response.key],
                     text: window[options.response.name][id][options.response.text]
                  });
               }

               return {results:result};
            }
         }
      };

      if (options.hasOwnProperty('noresults')) {
         if (configuration.hasOwnProperty('language')) {
            configuration.language.noResults = false;
         }
         else {
            configuration.language = {};
            configuration.language.noResults = false;
         }

         configuration.language.noResults = function() { return options.noresults };
      }
      currElem.select2(configuration);

      if ($(options.container).find('.select2-selection__arrow b')) {
         currElem.val(null).trigger('change');
         //currElem.select2("val","");

         $(options.container).find('.select2-selection__arrow b').remove();
         $(options.container).find('.select2-selection__arrow').append('<i class="fa fa-search"></i>');
      }

      if (callback) {
         callback();
      }
    }
})(jQuery);
