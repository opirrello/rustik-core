<?php
include_once('core/application/configs/environment.conf');
include_once('core/application/configs/system.conf');
include_once('core/application/configs/database.conf');
include_once('core/application/configs/custom.conf');
include_once('core/application/configs/forms.conf');

include_once(RF_CORE_HANDLERPATH . 'SystemHandler.php');

spl_autoload_register(array('SystemHandler', 'autoload'));

//SystemHandler::getInstance()->loadHandler('router');
//SystemHandler::getInstance()->loadHandler('template');
//SystemHandler::getInstance()->loadHandler('database');
//SystemHandler::getInstance()->loadHandler('messenger');

SystemHandler::getInstance()->loadFunctions();
SystemHandler::getInstance()->setInitials();

// CHecking CLI request vs URL request //
if (!SystemHandler::getInstance()->isCliRequest())
     list($controller, $method, $parameters) = RouterHandler::getInstance()->getRouteInfoFrom($_SERVER['REQUEST_URI']);
else list($controller, $method, $parameters) = RouterHandler::getInstance()->getRouteInfoFrom('/'.$_SERVER['argv'][1]);

$proceed = false;

if (!empty($controller) && !empty($method)) {
   $controller = ucfirst($controller).'Controller';

   if (SystemHandler::getInstance()->isDefinedController($controller)) {
      $obj_controller = new $controller;

      if ($obj_controller->_errorHandler->status != RF_ERR_FATAL) {
         if (method_exists($controller,$method)) {
            $proceed = true;
            //call_user_func_array(array($obj_controller,$method), $parameters);
         }
         else ErrorHandler::getInstance()->showError('inexistent-method', array($method, $controller));
      }
      else ErrorHandler::getInstance()->showError('general-failure', array($controller));
   }
   else ErrorHandler::getInstance()->showError('inexistent-area');
}
else ErrorHandler::getInstance()->showError('not-found');


/////////////////////////////////////////////////////////////////////////////////
// Template renderization ONLY if current work isn't under an AJAX request
/////////////////////////////////////////////////////////////////////////////////
if (SystemHandler::getInstance()->isAjaxRequest() || SystemHandler::getInstance()->isCliRequest()) {
   if (SystemHandler::getInstance()->isAjaxRequest()) {
      header("X-RF-isAjaxRequest: true");
   }

   if ($proceed) {
      $str_result = call_user_func_array(array($obj_controller,$method), $parameters);
   }

   if (!empty($str_result)) { 
      print $str_result;
   }
}
else {
   if ($proceed) {
      call_user_func_array(array($obj_controller,$method), $parameters);
   }

   TemplateHandler::getInstance()->exposeConstant('RF_DOMAIN');
   TemplateHandler::getInstance()->exposeConstant('RF_MAINTITLE');
   TemplateHandler::getInstance()->exposeConstant('RF_CORE_IMAGEURL');
   TemplateHandler::getInstance()->exposeConstant('RF_CORE_JSURL');
   TemplateHandler::getInstance()->exposeConstant('RF_CORE_CSSURL');

   //TemplateHandler::getInstance()->setVariable('src_logo', APP_IMAGEURL ."header_logo.png");
   TemplateHandler::getInstance()->render();
}

